<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY % html "IGNORE">
<![%html;[
<!ENTITY % print "IGNORE">
<!ENTITY docbook.dsl SYSTEM "/usr/share/sgml/stylesheets/docbook/html/docbook.dsl" CDATA dsssl>
]]>
<!ENTITY % print "INCLUDE">
<![%print;[
<!ENTITY docbook.dsl SYSTEM "/usr/share/sgml/stylesheets/docbook/print/docbook.dsl" CDATA dsssl>
]]>
]>
<style-sheet>
<style-specification id="print" use="docbook">
<style-specification-body>

(define %number-programlisting-lines%
  ;; Enumerate lines in a 'ProgramListing'?
  #t)

(define %linenumber-mod%
  ;; Controls line-number frequency in enumerated environments.
  1)

(define %refentry-generate-name%
  ;; Output NAME header before 'RefName'(s)?
  #f)

(define %mono-font-family%
  ;; The font family used in verbatim environments
  "AGaramond")

</style-specification-body>
</style-specification>
<style-specification id="html" use="docbook">
<style-specification-body>

(define %number-programlisting-lines%
  ;; Enumerate lines in a 'ProgramListing'?
  #t)

(define %linenumber-mod%
  ;; Controls line-number frequency in enumerated environments.
  1)

(define %refentry-generate-name%
  ;; Output NAME header before 'RefName'(s)?
  #f)

;; customize the html stylesheet

</style-specification-body>
</style-specification>
<external-specification id="docbook" document="docbook.dsl">
</style-sheet>

