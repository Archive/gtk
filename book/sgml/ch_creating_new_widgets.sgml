<!-- <!doctype chapter public "-//Davenport//DTD DocBook V3.0//EN" [
<!entity gtkmm-entities SYSTEM "entities.sgml">
%gtkmm-entities;
] -->
<chapter>
<docinfo>
<author><firstname>Guillaume</firstname><surname>Laurent</surname></author>
</docinfo>
<title>Creating New Widgets</title>
<para>
Creating new widgets is very easy with &gtkmm;. It is simply done
through C++ inheritance, that is by derivating a new class from one of
the currently existing &gtkmm; ones. Note that this works only for
widgets which are an aggregation of previously existing widgets
(e.g. like a dialog which would be made of a window holding a label,
an entry and a couple of buttons).
</para>
<para>
You can also create fully "new" widgets e.g. like a button or a menu,
by deriving from Gtk_Widget and overriding the *_event_impl() virtual
methods like gtk+ does. However this is currently way beyond the scope
of this document.
</para>

<para>
While C++ supports multiple inheritance, it's much more simple to have
your new class inheriting from a single &gtkmm; class, and have other
widgets as class data members. Other than the simplicity, it is also
conceptually much more fit. For instance, a dialog <emphasis/is/ a window (hence
would be derived from <classname/Gtk_Window/), and <emphasis/has/ a button, a label, an
entry, etc...
</para>
<para>
Let's start with a very simple example. Buttons often have tooltips
attached, therefore it would be nice to have a widget which would have
both, e.g. a "tipped button". Building this new widget is most simple,
just derive a new class from <classname/Gtk_Button/, and add a
<classname/Gtk_Tooltip/ as a
member:
</para>
<example><title/Tipped Button Header/
<programlisting role="C++">
// Very simple example of a button with a tooltip.

class Tipped_Button : public Gtk_Button 
{
  Gtk_Tooltips tip;
public:
  Tipped_Button(const string&amp; label, const string&amp; atip = "", const string&amp; tip_private = "");
  void set_tip(const string&amp; atip, const string&amp; tip_private);
};
</programlisting>
</example>

<para>
And its implementation :
</para>

<example><title/Tipped Button/
<programlisting role="C++">
Tipped_Button::Tipped_Button(const string&amp; label, const string&amp; atip, const string&amp; tip_private)
  : Gtk_Button(label)
{
  if(atip.length()) set_tip(atip, tip_private);
}

void Tipped_Button::set_tip(const string&amp; atip, const string&amp; tip_private)
{
  tip.set_tip(this, atip, tip_private);
}
</programlisting>
</example>

<para>
The <function>set_tip()</function> method is a <emphasis/delegation/ of
the one of <classname/Gtk_Tooltips/.
</para>

<para>
A sligthly more complex example, yet also much more common, is the one
of a window holding several widgets (a dialog is typical example). It
should look very familiar.
</para>

<example><title/Window containing Widgets/
<programlisting role="C++">
#include &lt;iostream&gt;
#include &lt;string&gt;
#include &lt;gtk--.h&gt;

// Gtk-- version of the table packing example from the gtk+ tutorial

Gtk_Main *mainwidget;

static int myquit(GdkEventAny*) {
  cout << "Quitting on delete event" << endl;
  mainwidget->quit();
  return 0;
}

class MyWin : public Gtk_Window
{
public:
  MyWin();
  
  Gtk_Table table;
  Gtk_Button b1, b2, bQuit;

  void callback(char* data);
};

MyWin::MyWin()
  : Gtk_Window(GTK_WINDOW_TOPLEVEL),
    table(2, 2, true),
    b1("button 1"),
    b2("button 2"),
    bQuit("Quit")
{
  set_title("Table");
  border_width(20);
  
  add(&amp;table);

  table.attach_defaults(b1, 0, 1, 0, 1);
  table.attach_defaults(b2, 0, 2, 0, 1);
  table.attach_defaults(bQuit, 0, 2, 1, 2);
  
  connect_to_function(delete_event, &amp;myquit);
  connect_to_method(b1.clicked, this, &amp;callback, "button 1");
  connect_to_method(b2.clicked, this, &amp;callback, "button 2");
  connect_to_signal(bQuit.clicked, delete_event,
		    static_cast&lt;GdkEventAny*&gt;(0));
  // the cast is needed to "help" template instantiation

  show_all();
}

void
MyWin::callback(char* data)
{
  cout << "Hello again - " << data << " was pressed" << endl;
}


int main(int argc, char *argv[])
{
  Gtk_Main myapp(&amp;argc, &amp;argv);
  mainwidget=&amp;myapp;
  MyWin mywin;

  mywin.show();
  connect_to_function(mywin.delete_event, &amp;myquit);

  myapp.run();
  return 0;
}
</programlisting>
</example>

<para>
Most of your &gtkmm; classes will look pretty much like this one, a
window with some widgets in it.
</para>

</chapter>
