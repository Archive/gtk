
{if !name /_callback/}
          <ListItem>
	{if func}
            <FuncSynopsis>
              <FuncDef>{filter sgmlent}{type}{endfilter}
                <Function ID="{filter sgmlattr}ref.l.{classname}..{mname}({args}){endfilter}">
		{if detail}
                  <Link LINKEND="{filter sgmlattr}ref.d.{classname}..{mname}({args}){endfilter}">
		{endif}
                  {mname}
		{if detail}
                  </Link>
                {endif}
                </Function>
              </FuncDef>
{filter arglist}{args}{endfilter}
		{if const}
              <FuncSynopsisInfo>const</FuncSynopsisInfo>
		{endif}
            </FuncSynopsis>
	{endif}
	{else}
            <Synopsis>
              <Type>{filter sgmlent}{type}{endfilter}</Type>
              <Parameter ID="{filter sgmlattr}ref.l.{classname}..{mname}{endfilter}">
		{if detail}
                <Link LINKEND="{filter sgmlattr}ref.d.{classname}..{mname}{endfilter}">
		{endif}
                  {mname}
		{if detail}
                </Link>
		{endif}
              </Parameter>
            </Synopsis>
        {endelse}
	{if brief}
            <Para>{brief}</Para>
	{endif}
          </ListItem>
{endif}
