
{filter detailpublic}
    <RefSect1><Title/Member Description/
      <RefSect2><Title/Public/
	{foreach public}
		{include memberdescitem.inc}
	{next}
      </RefSect2>

	{if !protected}
		{if !private}
    </RefSect1>
		{endif}
	{endif}
{endfilter}
{filter detailprotected}
	{filter notdetailpublic}
    <RefSect1><Title/Member Description/
	{endfilter}

      <RefSect2><Title/Protected/
	{foreach protected}
		{include memberdescitem.inc}
	{next}
      </RefSect2>

	{if !private}
    </RefSect1>
	{endif}
{endfilter}
{filter detailprivate}
	{filter notdetailpublic}
		{filter notdetailprotected}
    <RefSect1><Title/Member Description/
		{endfilter}
	{endfilter}
      <RefSect2><Title/Private/
	{foreach private}
		{include memberdescitem.inc}
	{next}
      </RefSect2>
    </RefSect1>
{endfilter}
