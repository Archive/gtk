
{if !name /_callback/}
	{if detail}
        <RefSect3 ID="crd.{filter memberid}{classname}::{mname}{if func}({args}){endif}{endfilter}">
          <Title><Function>
            <Link LINKEND="{filter sgmlattr}ref.l.{classname}..{mname}{if func}({args}){endif}{endfilter}">
              {if func}<Function>{mname}()</Function>{endif}
              {else}<Parameter>{mname}</Parameter>{endelse}
            </Link>
          </Title>
		{if func}
          <FuncSynopsis>
            <FuncDef>{type}<Function>{mname}</Function></FuncDef>
{filter arglist}{args}{endfilter}
			{if const}
                <FuncSynopsisInfo>const</FuncSynopsisInfo>
			{endif}
          </FuncSynopsis>
		{endif}
		{else}
          <Synopsis><Type>{type}</Type> <Parameter>{mname}</Parameter></Synopsis>
		{endelse}
	  <Para>{detail}</Para>
        </RefSect3>
	{endif}
{endif}
