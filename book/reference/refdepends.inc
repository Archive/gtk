
{if parents}

    <RefSect1><Title/Dependencies/
      <Refsect2><Title/Is Based On/
        <SimpleList>
	{foreach parent}
          <Member><Link LINKEND="{filter sgmlattr}ref:{name}{endfilter}">{name}</Link></Member>
        {next}
        </SimpleList>
      </RefSect2>

	{if !children}
    </RefSect1>
	{endif}
{endif}
{if children}
	{if !parents}
    <RefSect1><Title/Dependencies/
	{endif}

      <RefSect2><Title/Is Derived By/
        <SimpleList>
	{foreach child sort}
          <Member><Link LINKEND="{filter sgmlattr}ref:{name}{endfilter}">{name}</Link></Member>
       	{next}
	</SimpleList>
      </RefSect2>
    </RefSect1>
{endif}
