
{if public}
    <RefSect1><title/Member Indices/
      <RefSect2><title/Public/
        <ItemizedList>
	{foreach public}
		{if detail}{filter detailpublic}_true_{endfilter}{endif}
		{include memberlistitem.inc}
	{next}
        </ItemizedList>
      </RefSect2>

	{if !protected}
		{if !private}
    </RefSect1>
		{endif}
	{endif}
{endif}
{if protected}
	{if !public}
    <RefSect1><title/Member Indices/
	{endif}

      <RefSect2><title/Protected/
        <ItemizedList>
	{foreach protected}
		{if detail}{filter detailprotected}_true_{endfilter}{endif}
		{include memberlistitem.inc}
	{next}
        </ItemizedList>
      </RefSect2>

	{if !private}
    </RefSect1>
	{endif}
{endif}
{if private}
	{if !public}
		{if !protected}
    <RefSect1><title/Member Indices/
		{endif}
	{endif}
      <RefSect2><title/Private/
        <ItemizedList>
	{foreach private}
		{if detail}{filter detailprivate}_true_{endfilter}{endif}
		{include memberlistitem.inc}
	{next}
        </ItemizedList>
      </RefSect2>
    </RefSect1>
{endif}
