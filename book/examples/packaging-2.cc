/* packaging-2.sgml  example code for the Gtk-- book.
 * Copyright 1998  Marcus Brinkmann <Marcus.Brinkmann@ruhr-uni-bochum.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
#include <gtk--.h>

int main(int argc, char* argv[])
{
  Gtk_Main eventLoop(&argc, &argv);

  Gtk_Label word1("Hello");
  Gtk_Label symbol1(",");

  Gtk_Label word2("world");
  Gtk_Label symbol2("!");

  Gtk_HBox sentence;
  sentence.pack_start(word1);
  sentence.pack_start(symbol1);
  sentence.pack_end(symbol2);
  sentence.pack_end(word2);

  Gtk_Window helloWorldWindow;
  helloWorldWindow.add(sentence);
  helloWorldWindow.show_all();

  eventLoop.run();
}
