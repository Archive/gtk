/* checkbutton.sgml  example code for the Gtk-- book.
 * Copyright 1998  Marcus Brinkmann <Marcus.Brinkmann@ruhr-uni-bochum.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
#include <gtk--.h>

class ColorMix : public Gtk_HBox
{
public:
  ColorMix();

private:

  static const char *const Colors[8];

  Gtk_VBox colorResult;
    Gtk_Label message;		// "The resulting color is:"
    Gtk_Label color;		// "[not set]"

  Gtk_VBox colorSelection;
    Gtk_CheckButton base0;	// Colors[1<<0]
    Gtk_CheckButton base1;	// Colors[1<<1]
    Gtk_CheckButton base2;	// Colors[1<<2]

  void updateColor();
};

const char *const ColorMix::Colors[8] = {"Black",
  "Red", "Green", "Yellow", "Blue", "Magenta", "Cyan", "White"};

ColorMix::ColorMix()
 : message("The resulting color is:"),
   color("[not set]"),
   base0(Colors[1<<0]),
   base1(Colors[1<<1]),
   base2(Colors[1<<2])
{
  colorResult.pack_start(manage(new Gtk_Label("The resulting color should be:")));
  colorResult.pack_start(color);

  colorSelection.pack_start(base0);
  colorSelection.pack_start(base1);
  colorSelection.pack_start(base2);

  connect_to_method(base0.toggled, this, &ColorMix::updateColor);
  connect_to_method(base1.toggled, this, &ColorMix::updateColor);
  connect_to_method(base2.toggled, this, &ColorMix::updateColor);

  pack_start(colorSelection);
  pack_end(colorResult);

  updateColor();
}

void ColorMix::updateColor()
{
  color.set(Colors[ ((base0.get_active()?1:0)<<0)
                   +((base1.get_active()?1:0)<<1)
                   +((base2.get_active()?1:0)<<2)]);
}

int main(int argc, char* argv[])
{
  Gtk_Main eventLoop(&argc, &argv);

  ColorMix colorMixBox;

  Gtk_Window colorMix;
  colorMix.add(&colorMixBox);
  colorMix.show_all();

  eventLoop.run();
}
