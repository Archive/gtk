/* first-menu.sgml  example code for the Gtk-- book.
 * Copyright 1998  Marcus Brinkmann <Marcus.Brinkmann@ruhr-uni-bochum.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
#include <gtk--.h>

class Menu : public Gtk_MenuFactory {
public:
  Menu();
  Gtk_MenuFactory subFactory;
  static GtkMenuEntry menuItems[];
  static int nMenuItems;
};

GtkMenuEntry Menu::menuItems[] = {
  {"<Main>/File/New", 0, 0, 0},
  {"<Main>/File/Open", 0, 0, 0},
  {"<Main>/File/Save", 0, 0, 0},
  {"<Main>/File/<separator>", 0, 0, 0},
  {"<Main>/Options/A", 0, 0, 0},
  {"<Main>/Options/B", 0, 0, 0},
  {"<Main>/File/test/A", 0, 0, 0},
  {"<Main>/File/test/B", 0, 0, 0},
  {"<Main>/File/test/C", 0, 0, 0},
  {"<Main>/File/<separator>", 0, 0, 0},
  {"<Main>/File/Quit", 0, 0, 0}
};

int Menu::nMenuItems = sizeof(menuItems)/sizeof(menuItems[0]);

Menu::Menu()
 : Gtk_MenuFactory(GTK_MENU_FACTORY_MENU_BAR), subFactory(GTK_MENU_FACTORY_MENU_BAR)
{
  add_subfactory(&subFactory, "<Main>");
  add_entries(menuItems, nMenuItems);
}

//class Window : public Gtk_Window, Menu {
//public:
//  Window();
//};
//
//Window::Window()
//{
//  add(this);
//}

int main(int argc, char* argv[])
{
  Gtk_Main eventLoop(&argc, &argv);

  Menu menu;

  Gtk_Window helloWorldWindow;
  helloWorldWindow.add(menu.subFactory.get_menubar_widget());
  helloWorldWindow.show_all();

  eventLoop.run();
  return 0;
}
