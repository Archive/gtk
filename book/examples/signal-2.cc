/* signal-2.sgml  example code for the Gtk-- book.
 * Copyright 1998  Marcus Brinkmann <Marcus.Brinkmann@ruhr-uni-bochum.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */
#include <gtk--.h>

static void toggleLabel(Gtk_Label* theLabel)
{
static bool state=true;

  theLabel->set(state?"Howdy!":"Yow!");
  state = !state;
}

int main(int argc, char* argv[])
{
  Gtk_Main eventLoop(&argc, &argv);
  
  Gtk_Label helloWorldLabel("Greetings...");
  
  Gtk_Button helloWorldButton;
  helloWorldButton.add(helloWorldLabel);
  connect_to_function(helloWorldButton.clicked, &toggleLabel, &helloWorldLabel);
  
  Gtk_Window helloWorldWindow;
  helloWorldWindow.add(helloWorldButton); 
  helloWorldWindow.show_all();
  eventLoop.run();
}
