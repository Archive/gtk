From: Todd Dukes <tdukes@ibmoto.com>
Subject: [gtk-list] gtk-- - delete causes sigsegv
To: tdukes@ibmoto.com, Tero Pulkkinen <terop@students.cc.tut.fi>,
        gtk <gtk-list@redhat.com>
Date: Wed, 13 May 1998 15:44:09 -0500
Gnus-Warning: This is a duplicate of message <355A0618.FC472DAD@ibmoto.com>
Organization: Motorola Somerset
Resent-From: gtk-list@redhat.com
Reply-To: gtk-list@redhat.com
X-From-Line: gtk-list-request@redhat.com  Wed May 13 23:44:45 1998
Return-Path: <gtk-list-request@redhat.com>
Received: from mail2.redhat.com (mail2.redhat.com [199.183.24.247])
	by students.cc.tut.fi (8.8.5/8.8.5) with SMTP id XAA03458
	for <terop@students.cc.tut.fi>; Wed, 13 May 1998 23:44:44 +0300 (EET DST)
Received: (qmail 22687 invoked by uid 501); 13 May 1998 20:44:14 -0000
Resent-Date: 13 May 1998 20:44:14 -0000
Resent-Cc: recipient list not shown: ;
MBOX-Line: From gtk-list-request@redhat.com  Wed May 13 16:44:14 1998
Sender: tdukes@ibmoto.com
Message-ID: <355A0618.FC472DAD@ibmoto.com>
X-Mailer: Mozilla 4.03 [en] (X11; I; SunOS 5.6 sun4u)
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit
Resent-Message-ID: <"8YpiP2.0.pX5.TOWMr"@mail2.redhat.com>
X-Mailing-List: <gtk-list@redhat.com> archive/latest/574
X-Loop: gtk-list@redhat.com
Precedence: list
Resent-Sender: gtk-list-request@redhat.com
X-URL: http://www.redhat.com/linux-info/gtk/gtk-list/
Lines: 99
Xref: assari.cc.tut.fi Mail.gtk-list:3508 Mail.own:822

I try this simple application and get a sigsegv when I press the close
button. I saw in your code for the destructor the comment that you hide
the window to prevent the sigsegv from occuring. I must be missing
something
that should be called before deleting the pointer.

How should I go about deleting windows?

thanks
Todd

/*
The following command should make a binary called 'deleteTest'

 g++ -o deleteTest `gtk-config --cflags` `gtk-config --libs` -lgtkmm deleteTest.C

 */

#include <stream.h>
#include <gdk/gdkx.h>
#include <gtk--.h>

static char * pc_rcs_h = "global_h";
static char * pc_rcs = "$Id$";

#define USE(var) static void * use_##var = (void *) var
USE( pc_rcs_h);
USE( pc_rcs);

class SecondWindow : public Gtk_Window {
public:
  SecondWindow() : label("second window") { label.show(); add(&label);show();}
  Gtk_Label label;
};

class MyWindow : public Gtk_Window {
public:
  MyWindow();
  Gtk_Button open; /* create a child window */
  Gtk_Button close; /* delete this window */
  Gtk_Button quit;
  Gtk_VBox   vbox;
  void openPushed();
  void closePushed();
  SecondWindow * secondWindow;
};

MyWindow::MyWindow():
  open("open"),
  close("close"),
  quit("quit"),
  secondWindow(0) {
  open.show();
  close.show();
  quit.show();
  vbox.show();
  add(&vbox);
  vbox.pack_start(&open);
  vbox.pack_start(&close);
  vbox.pack_start(&quit);
  connect_to_method( open.clicked, this, &(MyWindow::openPushed));
  connect_to_method( close.clicked, this, &(MyWindow::closePushed));
  connect_to_method( quit.clicked, Gtk_Main::instance(), &Gtk_Main::quit);
}

void MyWindow::openPushed() {
  secondWindow = new SecondWindow;
  secondWindow->show();
}

void MyWindow::closePushed() {
  delete secondWindow;
  secondWindow = 0;
}

int main(int argc, char *argv[]) {
  Gtk_Main m(&argc, &argv);
  XSynchronize( GDK_DISPLAY(), False); /* true makes synchronous,
                                            false is asynchronous */
  MyWindow myWindow;
  myWindow.show();
  m.run();
  return 0;
}


/* ==============================================================
   | Todd Dukes                      E-MAIL:  tdukes@ibmoto.com |
   | Motorola Somerset                  Phone:   (512) 424-8008 |
   | MS OE70                                                    |
   | 6300 Bridgepoint Parkway Building #3                       |
   | Austin, Texas 78730                                        |
   ==============================================================*/



-- 
To unsubscribe: mail -s unsubscribe gtk-list-request@redhat.com < /dev/null

