From: Some Guy <guy@mikepery.pr.mcs.net>
Subject: [gtk-list] Gtk--: G_List quirks
To: gtk-list@redhat.com
Date: Sat, 1 Aug 1998 11:36:29 -0500 (CDT)
Resent-From: gtk-list@redhat.com
Reply-To: gtk-list@redhat.com
X-From-Line: gtk-list-request@redhat.com  Sat Aug  1 19:39:02 1998
Return-Path: <gtk-list-request@redhat.com>
Received: from mail2.redhat.com (mail2.redhat.com [199.183.24.247])
	by students.cc.tut.fi (8.8.5/8.8.5) with SMTP id TAA02906
	for <terop@students.cc.tut.fi>; Sat, 1 Aug 1998 19:39:01 +0300 (EET DST)
Received: (qmail 29974 invoked by uid 501); 1 Aug 1998 16:38:59 -0000
Resent-Date: 1 Aug 1998 16:38:59 -0000
Resent-Cc: recipient list not shown: ;
MBOX-Line: From gtk-list-request@redhat.com  Sat Aug  1 12:38:58 1998
Message-ID: <Pine.LNX.3.96.980801110913.6295A-100000@mikepery.pr.mcs.net>
MIME-Version: 1.0
Content-Type: TEXT/PLAIN; charset=US-ASCII
Resent-Message-ID: <"MMGrc2.0.KJ7.YIqmr"@mail2.redhat.com>
X-Mailing-List: <gtk-list@redhat.com> archive/latest/990
X-Loop: gtk-list@redhat.com
Precedence: list
Resent-Sender: gtk-list-request@redhat.com
X-URL: http://archive.redhat.com/gtk-list/
Lines: 73
Xref: assari.cc.tut.fi Mail.gtk-list:5621

(Tero, this is for you, I just posted here so everyone could have a look)

I was looking at the new G_List addon for gtk--, when I came across (I
think) a potential problem.  The G_List class is defined (partly) as
follows:

class G_List
{ 
  GList *list;
...
  G_List* next() { if(list->next) list = list->next; return this; }
  G_List* prev() { if(list->prev) list = list->prev; return this; }
...
}

The problem arises when you are using the G_List::next() and prev()
functions, thinking they will return a pointer to the next/prev node in
the list.  However, they don't only do that.  They first modify the glib
object, then do return this;.  This would work fine for someone doing the
following:
   for (list = container->children();
	list->glist() != NULL;
	list = list->next()) ...
Then it works as expected.  But what if someone only wanted a temporary
peek at the next/prev node?  For example:
   g_print("prev: %s\n", list->prev()->data());
   g_print("this: %s\n", list->data());
   g_print("next: %s\n", list->next()->data());
The first statement has the effect of doing
   g_print("prev: %s\n", (list = list->prev())->data);
as well as the last statement, which is surely not what you want in this
situation.  I'm not sure how often it will come up, but it probably will
show up some time.

I think we could solve this problem by just treating GList as a node.  For
example, redefine the G_List class this way:

typedef GList G_Node;
class G_List
{ 
  G_Node* pnode;
...
  G_List(G_Node* pnode) : pnode(pnode)
    { }
...
  G_Node* next() { return pnode->next; }
  G_Node* prev() { return pnode->prev; }
  G_Node* node() { return pnode; }
...
}

This makes it just as easy to traverse lists by doing the following:

   G_List list(glib_glist);
   for (G_Node *lptr = list->node(); lptr != NULL; lptr = lptr->next)
      printf("data: %s\n", lptr->data);

Because you are returning a G_Node, and not a G_List, you don't have to
worry about the G_List being modified or anything.

Just my $.02

---
Matt Perry [guy@mikepery.linuxos.org]

QOTD:	I looked out my window, and saw Kyle Pettys' car upside down,
	then I thought 'One of us is in real trouble'.
		-- Davey Allison, on a 150 m.p.h. crash
---

-- 
To unsubscribe: mail -s unsubscribe gtk-list-request@redhat.com < /dev/null

