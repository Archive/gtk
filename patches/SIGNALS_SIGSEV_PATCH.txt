From: Allan Rae <rae@elec.uq.edu.au>
Subject: [GTK--]  Signal bug, test program and fix.
To: terop@modeemi.cs.tut.fi
Date: Tue, 23 Jun 1998 15:45:11 +1000 (EST)
X-From-Line: rae@elec.uq.edu.au  Tue Jun 23 08:45:56 1998
Return-Path: <rae@elec.uq.edu.au>
Received: from vahti.cc.tut.fi (root@mail.cc.tut.fi [130.230.25.11])
	by students.cc.tut.fi (8.8.5/8.8.5) with ESMTP id IAA20624
	for <terop@assari.cc.tut.fi>; Tue, 23 Jun 1998 08:45:51 +0300 (EET DST)
Received: from modeemi.modeemi.cs.tut.fi (root@modeemi.cs.tut.fi [130.230.11.120])
	by vahti.cc.tut.fi (8.9.0/8.9.0) with ESMTP id IAA27580
	for <terop@cc.tut.fi>; Tue, 23 Jun 1998 08:45:38 +0300 (EET DST)
Received: from s4.elec.uq.edu.au (rae@s4.elec.uq.edu.au [130.102.96.4])
	by modeemi.modeemi.cs.tut.fi (8.9.0/8.9.0) with ESMTP id IAA04860
	for <terop@modeemi.cs.tut.fi>; Tue, 23 Jun 1998 08:45:34 +0300 (EET DST)
Received: from localhost (rae@localhost) by s4.elec.uq.edu.au (8.8.3/8.8.3) with SMTP id PAA19747 for <terop@modeemi.cs.tut.fi>; Tue, 23 Jun 1998 15:45:12 +1000 (EST)
X-Authentication-Warning: s4.elec.uq.edu.au: rae owned process doing -bs
Message-ID: <Pine.GSO.3.96.980623152332.10572B-100000@s4.elec.uq.edu.au>
MIME-Version: 1.0
Content-Type: TEXT/PLAIN; charset=US-ASCII
Lines: 150
Xref: assari.cc.tut.fi Mail.own:1096

Hi,

I've found a bug in the signalling code that results in a segfault.
It occurs when a connected function/method disconnects itself during a
signal activation.  Only takes a couple of lines to solve (see the
attached patch) because its just a matter of the list being modified while
signal is traversing the list.

I've also attached a test program which you can include in the gtk--
distribution if you like.  It is a cutdown version of a mechanism I am
using to close a bunch a dialog boxes when the data they depend upon (in
this case an editor buffer) isn't available.  The editor in question is
LyX.  We're using signals in a number of ways to achieve gui independence.

Cheers,
Allan.


gtk--sigcpp.h.patch  (from 0.9.7)
-----cut form here-----------------------------------------------------
--- /usr/include/gtk--sigcpp.h	Wed Jun 17 00:40:33 1998
+++ gtk--sigcpp.h	Tue Jun 23 06:01:37 1998
@@ -50,9 +50,11 @@
   }
   rettype operator()() {
     Connection_impl *c=connectionlist.begin();
+    Connection_impl *tmp;
     while(c!=connectionlist.end()) {
+      tmp=c->next;
       ( (Abstractslot0<rettype>*)c )->call();
-      c=c->next;
+      c=tmp;
     }
   }
 };
@@ -309,9 +311,11 @@
   } \
   rettype operator()(ARG_BOTH) { \
     Connection_impl *c=connectionlist.begin(); \
+    Connection_impl *tmp; \
     while(c!=connectionlist.end()) { \
+      tmp=c->next; \
       ( (Abstractslot##NUM<rettype,ARG_TYPE>*)c )->call(ARG_NAME); \
-      c=c->next; \
+      c=tmp; \
     } \
   } \
 }; \
------to here------------------------------------------------------------

test_signals.C
------cut from here------------------------------------------------------
/* 
 * Copyright (C) 1998
 * Allan Rae <rae@csee.uq.edu.au>
 * GPL Version 2.
 */

#define HAVE_MUTABLE

#include <gtk--sigcommon.h>
#include <gtk--sigcpp.h>
#include <stdio.h>

class TestSignal
{
 public:
   TestSignal()
     {}
   ~TestSignal()
     {}
   Signal0 signalMethod;
};

class Test : public virtual Gtk_Signal_Base
{
 public:
   bool connected;
   TestSignal *tS;
   Connection m;

   Test(TestSignal *ts)
     : connected(false), tS(ts)
     {
	m = connect_to_method(tS->signalMethod,
			      this,
			      &Test::method);
	connected = true;
     }
	
   ~Test()
     {
	if (connected) {
	   printf("destructor has ");
	   method();
	}
     }
   
   void method()
     {
	printf("method called\n");
	m.disconnect();
	connected = false;
     }
   
   void reconnect()
     {
	if (!connected) {
	   m = connect_to_method(tS->signalMethod,
				 this,
				 &Test::method);
	   connected = true;
	} else {
	   printf ("\talready connected\n");
	}
     }
};

int main()
{
   TestSignal ts;
   Test *a = new Test(&ts);
   Test *b = new Test(&ts);
   Test *c = new Test(&ts);

   ts.signalMethod();

   a->reconnect();
   Test *d = new Test(&ts);
   b->reconnect();
   
   ts.signalMethod();
   
   a->reconnect();
   b->reconnect();
   c->reconnect();
   d->reconnect();
   Test *e = new Test(&ts);
   
   delete a;
   delete b;
   delete c;
   delete d;
   delete e;
   
   return 0;
}
------to here-------------------------------------------------------


