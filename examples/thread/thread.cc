/*-------------------------------------------------------------------------
 * Filename:      gtkmm-thread.cc
 * Version:       0.1.3
 * Copyright:     Copyright (C) 1999, Erik Mouw
 * Copyright:     Copyright (C) 2000, Maurizio Umberto Puxeddu 
 * Author:        Erik Mouw <J.A.K.Mouw@its.tudelft.nl>
 * Gtk-- version: Maurizio Umberto Puxeddu <umbpux@tin.it>
 * Description:   GTK-- threads example. 
 * Created at:    Sun Oct 17 21:27:09 1999
 * Modified by:   Maurizio Umberto Puxeddu <umbpux@tin.it>
 * Modified at:   Sat Jan 22 21:48:19 2000
 *-----------------------------------------------------------------------*/
/*
 * Compile with:
 *
 * g++ -o gtkmm-thread gtkmm-thread.cc `gtkmm-config --cflags --libs` `gtk-config --libs gthread`
 * or maybe
 * g++ -o gtkmm-thread gtkmm-thread.cc `gtkmm-config --cflags --libs gthread--` 
 *
 */

#include <iostream>

#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include <gtk--/main.h>
#include <gtk--/window.h>
#include <gtk--/label.h>
#include <gtk--/box.h>
#include <gtk--/button.h>

#include <sigc++/thread.h>

class Opinionist : public SigC::Threads::Thread
{
public:
  Opinionist(Gtk::Label &label, const Gtk::string &opinion);
  ~Opinionist();
  void stop(void);
  void join();
  int kill(int sig = 2);
  
  bool isRunning(void);
  void isRunning(bool x);
private:
  SigC::Threads::Mutex _statusMutex;
  bool _isRunning;
  Gtk::string _opinion;
  Gtk::Label *_label;
  
  void *main(void *);
};

inline
bool
Opinionist::isRunning(void)
{
  bool r;

  _statusMutex.lock();
  r = _isRunning;
  _statusMutex.unlock();

  return r;
}

inline
void
Opinionist::isRunning(bool x)
{
  _statusMutex.lock();
  _isRunning = x;
  _statusMutex.unlock();
}

Opinionist::Opinionist(Gtk::Label &label, const Gtk::string &opinion) :
  _label(&label), _opinion(opinion), _isRunning(true)
{
}

inline
void
Opinionist::stop(void)
{
  isRunning(false);
}

inline
void
Opinionist::join(void)
{
  pthread_join(thread_, NULL);
}

inline
int
Opinionist::kill(int sig = 2)
{
  return pthread_kill(thread_, sig);
}

void *
Opinionist::main(void *)
{
  int sleepTime;

  while(isRunning())
    {
      sleepTime = int(3 * (float(rand())/RAND_MAX)) + 1;
     
      // get GTK thread lock
      gdk_threads_enter();
      
      // set label text
      _label->set_text(_opinion);

      // release GTK thread lock
      gdk_threads_leave();

      // sleep a while
      sleep(sleepTime);
    }
  
  return 0;
}

Opinionist::~Opinionist()
{
  kill();
  join();
}

//------------------

class MyWindow : public Gtk::Window
{
public:
  MyWindow();
private:
  Gtk::HBox box;
  Gtk::Label label;
  Opinionist *yesThread, *noThread;

  Gtk::Button button;
  void softExit(void);
  gint delete_event_impl(GdkEventAny*);
};

MyWindow::MyWindow() :
  label("And now for something completely different...")
  , button("quit")
{
  set_border_width(10);
  add(box);

  label.set_usize(150,30);
  button.set_usize(100,30);

  box.pack_start(label);
  box.pack_start(button);
  button.pressed.connect(slot(this, &MyWindow::softExit));

  show_all();

  // create the threads
  yesThread = new Opinionist(label, "Yes, it is!");
  noThread = new Opinionist(label, "No, it isn't!");

  yesThread->start();
  noThread->start();
}

gint
MyWindow::delete_event_impl(GdkEventAny*)
{
  delete yesThread;
  delete noThread;

  Gtk::Main::quit();
  return 0; 
}

void
MyWindow::softExit(void)
{
  yesThread->stop();
  noThread->stop();

  yesThread->join();
  noThread->join();

  Gtk::Main::quit();
}

// ----------------

int main(int argc, char *argv[])
{
  // init threads
  // NOTA BENE: this *MUST* be placed here, before any other statement.
  // At the time g_thread_init() must be the first function call in a
  // Gtk+ threaded application. So you can't even use global object that
  // call their CTOR before the program enters main()
  
  g_thread_init(NULL);

  Gtk::Main eventLoop(argc, argv);
  
  // init random number generator
  srand((unsigned int)time(NULL));
  
  // create a window
  MyWindow myWindow;
  
  // enter the GTK main loop
  gdk_threads_enter();
  eventLoop.run();
  gdk_threads_leave();
  
  return(0);
}
