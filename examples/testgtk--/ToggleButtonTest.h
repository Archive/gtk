/*
** ===========================================================================
** $RCSfile$
** $Revision$
** $Date$
** $Author$
** ===========================================================================
*/

#include "TestFixture.h"

#ifndef ToggleButtonTest_h
#define ToggleButtonTest_h "$Id$"

/*
** Gtk::ToggleButton
*/

class ToggleButtonTest : public TestFixture 
{
public:
  static TestFixture *  create ();
  virtual              ~ToggleButtonTest ();
  virtual void          destroyTest ();
private:
                        ToggleButtonTest ();
  //functions
  // data
  static ToggleButtonTest * theTest;
  Gtk::ToggleButton          button1;
  Gtk::ToggleButton          button2;
  Gtk::ToggleButton          button3;
};

inline
ToggleButtonTest::~ToggleButtonTest ()
{
}

#endif
