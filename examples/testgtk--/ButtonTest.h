/*
** ===========================================================================
** $RCSfile$
** $Revision$
** $Date$
** $Author$
** ===========================================================================
*/

/*
** Gtk::Button
*/

#include "TestFixture.h"

#ifndef ButtonTest_h
#define ButtonTest_h "$Id$"


class ButtonTest : public TestFixture 
{
public:
  static TestFixture *  create ();
  virtual              ~ButtonTest () { };
  virtual void          destroyTest ();
private:
                        ButtonTest ();
  // functions
  void                  toggleShowButton ( int buttonIndex );
  // data
  static ButtonTest * theTest;
  Gtk::VBox            box2;
  Gtk::Table           table;
  Gtk::Button         *button [ 9 ];
};

#endif
