#include <iostream>
#include <vector>
#include <gtk--/box.h>
#include <gtk--/button.h>
#include <gtk--/ctree.h>
#include <gtk--/window.h>
#include <gtk--/main.h>
#include <gtk--/box.h>

#include "dirOpened.xpm" 
#include "dirClosed.xpm"
#include "dirOpenedF.xpm" 
#include "dirClosedF.xpm"

using std::cout;
using std::endl;
using SigC::bind;
using SigC::slot;
using std::vector;

class AppWindow: public Gtk::Window  
  {
    Gtk::CTree*     eTree;
    Gtk::VBox*      eBox;
    Gtk::Button*    eButtonA;
    Gtk::Button*    eButtonB;    
    Gtk::Button*    eButtonC;    
    Gtk::CTree::Row eRow;
    public:
      AppWindow();
      virtual ~AppWindow();
      virtual gint delete_event_impl (GdkEventAny*);

      void e_treeSelectRow (Gtk::CTree::Row row,gint column);            
      void e_buttonPress   (short eButton);
  };

AppWindow::AppWindow() : Gtk::Window() 
  {
    vector<char*> titles;
    titles.push_back("Items");
    titles.push_back("Column2");
    eTree    = manage(new Gtk::CTree(titles));
    eBox     = manage(new Gtk::VBox());
    eButtonA = manage(new Gtk::Button("Set is_leaf to true"));
    eButtonB = manage(new Gtk::Button("Set spacing to 15"));
    eButtonC = manage(new Gtk::Button("Fill Pixmap"));

    eTree->column(0).set_width(100);
    eTree->set_usize(300,300);    
    eTree->set_expander_style(GTK_CTREE_EXPANDER_NONE);
    eTree->set_show_stub(true);

    eBox->pack_start(*eTree,true,true,0);
    eBox->pack_start(*eButtonA,true,true,0);
    eBox->pack_start(*eButtonB,true,true,0);
    eBox->pack_start(*eButtonC,true,true,0);
    add(*eBox);

    show_all();

    Gdk_Pixmap pixOpened;
    Gdk_Bitmap maskOpened;
    Gdk_Pixmap pixClosed; 
    Gdk_Bitmap maskClosed;
    pixOpened.create_from_xpm_d(get_window(),maskOpened,Gdk_Color("#FFFFFF"),opened_xpm);
    pixClosed.create_from_xpm_d(get_window(),maskClosed,Gdk_Color("#FFFFFF"),closed_xpm);
    { 
      using namespace Gtk::CTree_Helpers;
      RowList::iterator i;
      vector<char*> item;
      item.push_back("fruit");
      item.push_back("1");
      eTree->rows().push_back(Element(item,pixClosed,maskClosed,pixOpened,maskOpened));

      i=--eTree->rows().end();
      item[0]="peaches";
      i->subtree().push_back(Element(item,pixClosed,maskClosed,pixOpened,maskOpened));
      item[0]="apples";
      i->subtree().push_back(Element(item,pixClosed,maskClosed,pixOpened,maskOpened));
      item[0]="pears";
      i->subtree().push_back(Element(item,pixClosed,maskClosed,pixOpened,maskOpened));

      item[0]="animals";
      eTree->rows().push_back(BranchElem(item,pixClosed,maskClosed,pixOpened,maskOpened));

      cout << "traverse row list (forward)"<<endl;
      for (RowList::iterator i1=eTree->rows().begin(); 
            i1!=eTree->rows().end(); ++i1)
      cout << (*i1)[0].get_text() <<endl; 
      cout << "traverse tree list (forward)"<<endl;
      for (TreeList::iterator i1=eTree->tree().begin();i1!=eTree->tree().end();++i1)
          cout << (*i1)[0].get_text() <<endl; 
    } 
    eTree->tree_select_row.connect(slot(this,&AppWindow::e_treeSelectRow));
    eButtonA->clicked.connect(bind<short>(slot(this,&AppWindow::e_buttonPress),1));
    eButtonB->clicked.connect(bind<short>(slot(this,&AppWindow::e_buttonPress),2));
    eButtonC->clicked.connect(bind<short>(slot(this,&AppWindow::e_buttonPress),3));
  }

AppWindow::~AppWindow() { } 

void AppWindow::e_treeSelectRow (Gtk::CTree::Row row,gint column) 
  {
    if (eRow == row) 
      cout << "------> Same Row clicked!" << endl;
    else 
      {
        eRow = row;
        if (eRow != NULL) 
          { 
            cout << "-------------" << endl;
            gint eSpace = eRow.get_spacing();
            cout << "Spacing:  " << eSpace << endl
                 << "Leaf:     " << eRow.is_leaf() << endl
                 << "expanTed: " << eRow.is_expanded() << endl;
          }
      }
  }

void AppWindow::e_buttonPress(short eButton) 
  {
    switch (eButton) 
      {
        case 1:
            eRow.set_leaf(true);
            break;
        case 2:
            eRow.set_spacing(15);
            break;
        case 3:
            Gdk_Pixmap pixOpenedF;
            Gdk_Bitmap maskOpenedF;
            Gdk_Pixmap pixClosedF; 
            Gdk_Bitmap maskClosedF;
            pixOpenedF.create_from_xpm_d(get_window(),maskOpenedF,Gdk_Color("#FFFFFF"),openedF_xpm);
            pixClosedF.create_from_xpm_d(get_window(),maskClosedF,Gdk_Color("#FFFFFF"),closedF_xpm);
            eRow.set_opened(pixOpenedF,maskOpenedF);
            eRow.set_closed(pixClosedF,maskClosedF);
            break;        
      }
  }

gint AppWindow::delete_event_impl (GdkEventAny*) 
  {
    Gtk::Main::quit();
    return 0;
  }

int main (int argc, char *argv[]) 
  {
    Gtk::Main kit(argc, argv);
    AppWindow arrows;

    kit.run ();
    return 0;
  }
