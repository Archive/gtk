#include <iostream>
#include <gtk--/box.h>
#include <gtk--/main.h>
#include <gtk--/menu.h>
#include <gtk--/menuitem.h>
#include <gtk--/item.h>
#include <gtk--/menubar.h>
#include <gtk--/label.h>
#include <gtk--/window.h>
#include <gdk--/pixmap.h>
#include <gdk--/bitmap.h>

//class PixmapMenuItem:public Gtk::MenuItem {
//	private:
//		Gdk_Pixmap pix;
//		Gdk_Bitmap mask;
//	public:
//		PixmapMenuItem(const Gtk::string &label,gfloat x=0.0,gfloat y=0.5);			
//};
//
//PixmapMenuItem::PixmapMenuItem(const Gtk::string &label,gfloat x=0.0,gfloat y=0.5)
//	: MenuItem(label,x,y)
//{
//	GtkMenuItemClass* eGMIC; 
//	memcpy(eGMIC,(GtkMenuItemClass*)(((GtkObject*)(gtkobj()))->klass),sizeof(*((GtkMenuItemClass*)(((GtkObject*)(gtkobj()))->klass))));
//	eGMIC->toggle_size  = 12;
//	((GtkMenuItemClass*)(((GtkObject*)(gtkobj()))->klass)) = eGMIC;
//}

using namespace Gtk;
using std::cout;
using std::endl;
using SigC::slot;
using SigC::bind;

void void_void_cb(void)       { cout << "reached void_void_callback" << endl; }
void void_int_cb(int num)     { cout << "reached void_int_callback, num = " << num << endl; }
void edit_cb(Gtk::string action)   { cout << "reached edit_callback, requested action " << action << endl; }
void toggle_cb(Gtk::string action) { cout << "reached toggle_callback, requested action " << action << endl; }
void destroy_cb(void)         { Main::quit(); }

int main(int argc, char *argv[]) {
   Main kit(argc, argv);

   MenuBar *menubar = manage( new MenuBar());
   {
     using namespace Menu_Helpers;

		 // Create the file menu
     Menu    *menu_file      = manage( new Menu());
     MenuList& list_file     = menu_file->items();
  	 list_file.push_back(TearoffMenuElem(slot(void_void_cb)));
	   list_file.push_back(MenuElem("_New"));
			 list_file.back()->activate.connect(bind(slot(void_int_cb), 123));
     list_file.push_back(MenuElem("_Open", slot(void_void_cb)));
		   list_file.back()->set_state(GTK_STATE_INSENSITIVE);
     list_file.push_back(MenuElem("Save", bind(slot(void_int_cb), 234)));
     list_file.push_back(MenuElem("Save As",slot(void_void_cb)));
     list_file.push_back(MenuElem("_Close", CTL|'c', slot(void_void_cb)));
     list_file.push_back(SeparatorElem());
     list_file.push_back(MenuElem("_Quit", "<control>q", slot(destroy_cb)));

     // Create a submenu
     Menu *menu_sub = manage( new Menu());
     MenuList& list_sub = menu_sub->items();
     list_sub.push_back(MenuElem("Sub1"));
     list_sub.push_back(MenuElem("Sub2"));
     list_sub.push_back(MenuElem("Sub3"));

     // Create the edit menu
     Menu *menu_edit                = manage( new Menu());
     MenuList& list_edit     = menu_edit->items();
		 list_edit.push_back(TearoffMenuElem("<ctl><alt>e",slot(void_void_cb)));
     list_edit.push_back(MenuElem("Cut"));
     list_edit.push_back(MenuElem("Copy"));
     list_edit.push_back(MenuElem("Paste"));
     list_edit.push_back(MenuElem("Options",*menu_sub));

     // Create the example menu
     Menu *menu_examples = manage( new Menu());
     MenuList& list_examples = menu_examples->items();
     RadioMenuItem::Group gr;
     list_examples.push_back(RadioMenuElem(gr,"RadioItem Example 1",bind<Gtk::string>(slot(toggle_cb), "radio example1")));
     list_examples.push_back(RadioMenuElem(gr,"RadioItem Example 2",bind<Gtk::string>(slot(toggle_cb), "radio example2")));
     list_examples.push_back(RadioMenuElem(gr,"RadioItem Example 3",bind<Gtk::string>(slot(toggle_cb), "radio example3")));
	     static_cast<RadioMenuItem*>(list_examples.back())->set_active();
     list_examples.push_back(SeparatorElem());
     list_examples.push_back(CheckMenuElem("CheckItem Example",bind(slot(toggle_cb), (Gtk::string)"check example")));

     // Create the help menu
     Menu    *menu_help                = manage( new Menu());
     menu_help->items().push_back(MenuElem("About", slot(void_void_cb)));

     // Create the menu bar
     MenuList& list_bar = menubar->items();
     list_bar.push_front(MenuElem("_Help","<control>h",*menu_help));
     list_bar.front()->right_justify();
     menubar->items().push_front(MenuElem("E_xamples","<control>x",*menu_examples));
     menubar->items().push_front(MenuElem("_Edit","<control>e",*menu_edit));
     menubar->items().push_front(MenuElem("_File","<control>f",*menu_file));
   }

   // vbox
   VBox *vbox = manage( new VBox() );
   vbox->pack_start (*menubar, false, false);

   // label 
   Label *lBody = manage( new Label("<control>e: Edit Menu\n<control><alt>e: Torn Off Edit Menu") );
	 lBody->set_justify(GTK_JUSTIFY_LEFT);
   vbox->pack_end(*lBody);

	 // window
   Window *window = manage( new Window() );
   window->destroy.connect(slot(destroy_cb));
   window->set_title ("Tearoff MenuDemo");
	 window->set_usize(250,100);
   window->set_default_size (250, 100);
   window->add (*vbox);
   window->set_policy (false, true, false);
   window->show_all();

   kit.run();
}
