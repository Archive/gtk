#include <gtk--/combo.h>
#include <gtk--/window.h>
#include <gtk--/main.h>

class TestWindow : public Gtk::Window
  {
    public:
      Gtk::Combo m_combo;
      TestWindow();
      ~TestWindow();
    protected:
      gint delete_event_impl (GdkEventAny*);
  };

TestWindow::TestWindow()
  {
    add( m_combo );
 
    m_combo.get_entry()->set_editable(false);

    for(int at=0;at<6;at++)
        m_combo.get_list()
                    ->add(*manage(new Gtk::ListItem("Test List Item")));
    m_combo.get_list()->show_all();
    show_all();  // bug in gtk+ does not propagate show_all to list items
  }

TestWindow::~TestWindow()
  {}

gint TestWindow::delete_event_impl (GdkEventAny*)
  {
    Gtk::Main::quit();
    return 0;
  }

int main(int argc, char *argv[])
  {
    Gtk::Main myapp(argc, argv);
    TestWindow mainwnd;
    myapp.run();
    return 0;
  }

