/* dialog.cc
 * 
 * Demo to make a custom dialog.
 *
 * Copyright 2000 Karl Nelson
 *
 */


#include <gtk--/button.h>
#include <gnome--/app.h>
#include <gnome--/dialog.h>
#include <gnome--/main.h>

using SigC::slot;

void foo()
{
  cout << "hello world!" << endl;
}

void run()
{
  gchar *button_list[] = { "Ok", "Cancel", "Apply", 0 };
  Gnome::Dialog dialog("Question", button_list);
  dialog.set_default(0);
  dialog.connect(0,slot(&foo));
  cout << "user pressed " << dialog.run() << endl;
}

gint on_app_delete_event(GdkEventAny*)
{ 
  Gtk::Main::quit();
  return 0; 
}

int main(int argc, char* argv[])
{
  Gnome::Main kit("GnomeDialog", "0.1", argc, argv );
  Gnome::App  app("GnomeDialog", "Gnome Dialog");
  Gtk::Button button("Press Me!");

  button.clicked.connect(slot(run));
  app.set_contents(button);
  app.delete_event.connect(slot(&on_app_delete_event));
  app.show_all();
  
  kit.run();

  return 0;
}
