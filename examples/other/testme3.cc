#include <stdlib.h>
#include <stdio.h>

#include <gtk--.h>



class PipesTest : public Gtk::Window {
    Gtk::Label l;
    Gtk::Button b;
    Gtk::VBox *vb;
    Gtk::Text *text;
public:
  PipesTest() 
    : l("Hello World!"),
      b("Hello-button"),
      vb(new Gtk::VBox),
      text(new Gtk::Text)
    {
	add(*vb);
	vb->pack_start(l);
	vb->pack_start(b);
	vb->pack_start(*text);
	vb->show();
	l.show();
	b.show();
	text->show();
    }
    
    gint delete_event_impl(GdkEventAny*) { 
	Gtk::Main::quit(); return 0; 
    }

};


int main(int argc, char *argv[]) {
  Gtk::Main m(argc,argv);
  PipesTest *t=new PipesTest;
  t->show();
  m.run();
  printf("After run!\n");
  return(EXIT_SUCCESS);
}












