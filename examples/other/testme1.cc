#include <stdio.h>
#include <sigc++/bind.h>
#include <gtk--.h>

// Main idea of this test program is to test how connections work in gtk.
//
// It does not do anything too neat, just prints some dummy things to
// console.

class MyWin : public Gtk::Window
{
    Gtk::HBox box;
    Gtk::VBox vbox;
    Gtk::Frame frame;
    Gtk::Frame frame1;
    Gtk::Button b0,b1;
    Gtk::Tooltips btip;
    Gtk::Entry text;
    Gtk::ProgressBar bar;
    Gtk::Notebook book;
    Gtk::Label bm1,bm2,bm3;
    int num;
    
    // Connection-objects can be used for disconecting a connection.
    // it isnt necessary to know about these at all, they are here
    // for easier use of connect().
    Connection c1,c2,c3,c4;
    
public:  
    Signal1<void,GdkEventKey *> keyPressed;
    Signal1<void,guint> foo;
    Signal0<void> test;
    
private:
    
    void dohello(){ printf("Button pushed - number was %d\n",num);}
    void dohello_cb(void *)
    {
      printf("Button pushed_cb\n");}
    
    
    void keypress(GdkEventKey *event)
    { 
	// sending foo and keyPressed-signals.
	foo(10); keyPressed(event); 
	test();
	c1.disconnect(); // now we'll test disconnecting the connections.
	c2.disconnect();
	c3.disconnect();
	c4.disconnect();
	printf("Keypress was %d",event->keyval);
    }
    
public:
    virtual void doblah(guint a) { printf("Blah: %d",a); }
    void doblah_cb(guint a,void*) { printf("Blah_cb:%d ",a); }
    gint delete_event_impl(GdkEventAny*) { Gtk::Main::quit(); return 0; }  
    MyWin(gint num);
};

MyWin::MyWin(int num)
    : Gtk::Window(GTK_WINDOW_TOPLEVEL),
      box(true,0),
      vbox(true,0),
      frame("A1"),frame1("A2"),
      b0("The"),
      b1("\"Very sad life. Probably have very sad death. \nBut at least, there is symmetry.\""),
      bm1("Babylon5Quote#1"),bm2("Babylon5Quote#2"),bm3("\"May God stand between you and harm,\n in all the empty places where you must walk\""),
      num(num)
{
    // vbox
    add(book);
    book.pages().push_back(Gtk::Notebook_Helpers::TabElem(frame,bm1));
    book.pages().push_back(Gtk::Notebook_Helpers::TabElem(frame1,bm2));
    book.show();
    frame.add(vbox);
    vbox.add(box);
    vbox.add(bar);
    frame.show();
    frame1.show();
    frame1.add(b1);
    box.show();
    bar.show();
    vbox.show();
    bar.set_percentage(0.35);
    
    box.pack_start(bm3);
    
    // connect sig0 to c++ function
    test.connect(slot(this,&MyWin::dohello));
    test.connect(bind(slot(this,&MyWin::dohello_cb),(void*)0));
    
    // connect c++ signal to c++ function from gtk--
    c3=foo.connect(slot(this,&MyWin::set_border_width));
    
    // connect c++ signal to user function
    c4=foo.connect(slot(this,&MyWin::doblah));
    c2=foo.connect(bind(slot(this,&MyWin::doblah_cb),(void*)0));
    b1.clicked.connect(Gtk::Main::quit.slot());

    btip.set_tip(b1,"Press here to quit","A");
    b0.show();
    b1.show();
    bm3.show();
}

static void staticfunction(GdkEventKey* a) {
    printf("Staticfunction called! %p\n",a);
}

static void staticfunction_cb(GdkEventKey* a,void *) {
    printf("Staticfunction-cb called! %p\n",a);
}

Gtk::Main *mainwidget=0;

int
main(int argc, char *argv[])
{
    Gtk::Main myapp(argc, argv);
    mainwidget=&myapp;
    MyWin mywin(5);
    
    
    mywin.set_title("Notice!");

    mywin.show();
    
    // connecting to static function
    mywin.keyPressed.connect(slot(&staticfunction)); 
    mywin.keyPressed.connect(bind(slot(&staticfunction_cb),(void*)0));
    
    myapp.run();
    return 0;
}








