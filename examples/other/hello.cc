// here at the beginning we'd have:
// #include <hello.h>, but since we dont need it, we dont have it.
// But its place is in the beginning...

#include <gtk--/widget.h>
#include <gtk--/button.h>
#include <gtk--/window.h>
#include <gtk--/main.h>


//
// hello.cc
//
// This piece of hello world application shows many of the important
// features of gtk--.
//
// Watch for:
// 1) deriving widget from another
// 2) adding a widget inside another widget derived from Gtk::Container
// 3) connecting callbacks to their implementation 
// 4) showing widgets
// 5) overriding virtual members of existing widgets
// 6) instantiating widgets
// 7) initializing event loop
// 8) starting event loop
//

class HelloWorld : public Gtk::Window { // (1)
   Gtk::Button b;
public:
   HelloWorld() : b( "Hello World" ) {
      add( b ); // (2)
      b.clicked.connect(Gtk::Main::quit.slot()); // (3)
      b.show(); // (4)
   }
   gint delete_event_impl(GdkEventAny *) { // (5) 
      Gtk::Main::quit(); 
      return 0;
   } 
};


int main( int argc, char **argv )
{
   Gtk::Main m( &argc, &argv ); // (7)
   HelloWorld w; // (6)
	
   w.set_usize( 100,20 ); 
   w.show(); // (4)

   m.run(); // (8)
   return 0;
}


