#include <iostream.h>
#include <stdio.h>
#include <gtk--.h>

// Main idea of this test program is to test how connections work in gtk.
//
// It does not do anything too neat, just prints some dummy things to
// console.

class MyWin : public Gtk::Window
{
    Gtk::VBox box;
    Gtk::ScrolledWindow swin;
    Gtk::Button b;
    Gtk::Tooltips btip;
    Gtk::Entry text;
    Gtk::Viewport footext;
    
    
    // Connection-objects can be used for disconecting a connection.
    // it isnt necessary to know about these at all, they are here
    // for easier use of connect().
    Connection c1,c2,c3,c4;
    bool init;
public:  
    Signal1<void,GdkEventKey *> keyPressed;
    Signal1<void,gint> foo;
    Signal0<void> test;
    Signal0<void> test2;
    
private:
    Gtk::FileSelection *fs;
    gint selectionclosed(GdkEventAny*) {
	printf("Selectionclosed\n");
	// WHY DOESNT PASSING THIS RETURN VALUE WORK? It should NOT delete
	// GtkFileSelection object when this returns true... but it does
        // and I have no idea why... I'd guess gtk never gets this true..
        // needs investigating.

	// I think we now return it correctly, but...
	// returning false works, but it'll delete the dialog
	// retyrning true does something odd, the main window
	// nor future widget's resize events doesny work...
	//fs->hide();
	//fs=0;
	return true;
    }
    void buttoncanceled() {
	cout << "File NOT Selected " << fs->get_filename() << endl;
	fs->hide();
    }
    void buttonpressed() {
	cout << "File Selected " << fs->get_filename() << endl;
	fs->hide();
    }
    void showfileselection(){
	if (!fs) { 
	    // seems that creating new widget takes time.. 
	    // better do it once and then just hide and show that one
	    // this way you wont get million similar windows to the 
	    // screen either...
	    fs=new Gtk::FileSelection("File Selection");
	    fs->get_ok_button()->clicked.connect(slot(*this,&MyWin::buttonpressed));
	    fs->get_cancel_button()->clicked.connect(slot(*this,&MyWin::buttoncanceled));
	    fs->delete_event.connect(slot(*this,&MyWin::selectionclosed));
	}
	fs->show();
    }
    	
    void keypress(GdkEventKey *event)
    { 
	// sending foo and keyPressed-signals.
	foo(10); keyPressed(event); 
	test();
	c1.disconnect(); // now we'll test disconnecting the connections.
	c2.disconnect();
	c3.disconnect();
	c4.disconnect();
	printf("Keypress was %d\n",event->keyval);
    }
    
public:
    
    virtual gint expose_event_impl(GdkEventExpose* e) { // catch expose events. 
	printf("expose!\n");
	Gtk::Widget::expose_event_impl(e);
	return 0;
    }
    
    MyWin();
};

MyWin::MyWin()
    : Gtk::Window(GTK_WINDOW_TOPLEVEL),
      box(true,0),
      b("Push me"),
      init(false), fs(0)
{
    // vbox
    add(box);
    box.show();
    
    // button
    box.pack_start(b);
    box.pack_start(swin);
    swin.add(footext);
   
    b.clicked.connect(slot(*this,&MyWin::showfileselection));
   
    btip.set_tip(b,"Press this to get fileselection","private string");
    b.show();
    

    // text entry
    box.pack_start(text);
    text.show();
    footext.show();
    swin.show();
    
}

Gtk::Main *mainwidget;

int myquit(GdkEventAny*) {
    printf("MYQUIT\n");
    Gtk::Main::quit();
    return 0;
}

int
main(int argc, char *argv[])
{
    Gtk::Main myapp(&argc, &argv);
    mainwidget=&myapp;
    MyWin mywin;
    mywin.delete_event.connect(slot(&myquit));
    
    mywin.show();
    
    myapp.run();
    return 0;
}






