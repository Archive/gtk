
#include <stdio.h>
#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <gtk--.h>

class MainWindowClass : public Gtk::Window {
private:
  Gtk::HBox main_hbox;
  Gtk::HBox hbox;
  Gtk::Button button;
  Gtk::Toolbar toolbar;
  
  void quit_pressed_cb(void);
  void toolbar_button_cb(char *);
  
public:

   MainWindowClass (void);
  ~MainWindowClass (void);

};

Gtk::Main *app;

void MainWindowClass::toolbar_button_cb(char *c)
{
  printf("toolbar_button_cb : %s\n",c);
}

MainWindowClass::MainWindowClass(void) : Gtk::Window(GTK_WINDOW_TOPLEVEL),
  main_hbox(FALSE,0),
  hbox(FALSE,0),
  button("Quit")
{

  set_usize(400,50);
  
  add(main_hbox);

  main_hbox.add(hbox);
  
  hbox.pack_start(button,FALSE,FALSE,0);
  hbox.pack_start(toolbar,FALSE,FALSE,0);

  button.clicked.connect( slot(this,&MainWindowClass::quit_pressed_cb) );

  toolbar.tools().push_back(Gtk::Toolbar_Helpers::ButtonElem( 
			 "Click me", 
                          bind<char*>( slot(this,&MainWindowClass::toolbar_button_cb),
                              "'Click me' button"),
                         "toolbar btn",""));
  toolbar.tools().push_back(Gtk::Toolbar_Helpers::Space());

  toolbar.tools().push_back(Gtk::Toolbar_Helpers::ButtonElem(
			 "Click me too", 
			 bind<char*>( slot(this, &MainWindowClass::toolbar_button_cb),
                             "'Click me too' button"),
                         "other toolbar btn", ""));


  toolbar.tools().push_back(Gtk::Toolbar_Helpers::ToggleElem(
                         "Toggle me", 
			 bind<char*>( slot(this, &MainWindowClass::toolbar_button_cb), 
                             "This is from a toggle connector"),
                         "toggle duh", ""));

  toolbar.show();
  button.show();
  hbox.show();
  main_hbox.show();
}

MainWindowClass::~MainWindowClass(void)
{}

void MainWindowClass::quit_pressed_cb(void)
{
  Gtk::Main::quit();
}

int main(gint argc, gchar **argv)
{
  MainWindowClass *main_window;
  
  app = new Gtk::Main(argc,argv);

  main_window = new MainWindowClass;
  main_window->show();
  
  app->run();

  return(0);
}
