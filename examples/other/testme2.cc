#include <stdio.h>
#include <gtk--.h>
#include <sigc++/bind.h>

// Main idea of this test program is to test how connections work in gtk.
//
// It does not do anything too neat, just prints some dummy things to
// console.

class MyWin : public Gtk::Window
{
  Gtk::VBox box;
  Gtk::Button b;
  Gtk::Tooltips btip;
  Gtk::Entry text;
  
  int num;

  // Connection-objects can be used for disconecting a connection.
  // it isnt necessary to know about these at all, they are here
  // for easier use of connect().
  Connection c1,c2,c3,c4;
  Gtk::Main *main;
public:  
  Signal1<void,GdkEventKey *> keyPressed;
  Signal1<void,guint> foo;
  Signal0<void> test;
  
private:

  void dohello(){ printf("Button pushed - number was %d\n",num);}
  void dohello_cb(void *){printf("Button pushed_cb - number was %d\n",num);}

  gint keypress(GdkEventKey *event)
  { 
    // sending foo and keyPressed-signals.
    foo(10); keyPressed(event); 
    test();
    c1.disconnect(); // now we'll test disconnecting the connections.
    c2.disconnect();
    c3.disconnect();
    c4.disconnect();
    printf("Keypress was %d\n",event->keyval);
    return 0;
  }


public:
  virtual void doblah(guint /* a */ ) {  }
  void doblah_cb(guint /* a */, void*) { }

  virtual gint destroy_event_impl(GdkEventAny * /* o */) {
    printf("destroy_event_impl\n");
    return 0; 
  }

  
  MyWin(gint num, Gtk::Main *m);
};

MyWin::MyWin(int num, Gtk::Main *m)
  : Gtk::Window(GTK_WINDOW_TOPLEVEL),
    box(true,0),
    b("Push me"),
    num(num),
    main(m)
{
  // vbox
  add(box);
  box.show();
  
  // button
  box.pack_start(b);

  // connect gtk signal to c++ function
  b.clicked.connect(slot(this,&MyWin::dohello));
  b.clicked.connect(bind(slot(this,&MyWin::dohello_cb),(void*)0));

  // connect sig0 to c++ function
  test.connect(slot(this,&MyWin::dohello));
  test.connect(bind(slot(this,&MyWin::dohello_cb),(void*)0));

  // connect c++ signal to c++ function from gtk--
  c3=foo.connect(slot(this,&MyWin::set_border_width));

  // connect c++ signal to user function
  c4=foo.connect(slot(this,&MyWin::doblah));
  c2=foo.connect(bind(slot(this,&MyWin::doblah_cb),(void*)0));

  btip.set_tip(b,
	       "This button is connected using the new c++ signal handler code.");
  b.show();
  
  // text entry
  box.pack_start(text);
  text.show();
  text.key_press_event.connect(slot(this,&MyWin::keypress));
}

static void staticfunction(GdkEventKey* a) {
  printf("Staticfunction called! %p\n",a);
}

static void staticfunction_cb(GdkEventKey* a,void *) {
  printf("Staticfunction-cb called! %p\n",a);
}

static gint myquit(GdkEventAny*) {
  printf("MYQUIT\n");
  Gtk::Main::quit();
  return 0;
}

int
main(int argc, char *argv[])
{
  Gtk::Main myapp(argc, argv);
  MyWin mywin(5,&myapp);
  
  mywin.delete_event.connect(slot(&myquit));
  mywin.show();

  // connecting to static function
  mywin.keyPressed.connect(slot(&staticfunction)); 
  mywin.keyPressed.connect(bind(slot(&staticfunction_cb),(void*)0)); 
  myapp.run();
  return 0;
}








