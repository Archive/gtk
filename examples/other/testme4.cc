
#include <gdk/gdkx.h>
#include <gtk--.h>

Gtk::Main *mainwidget=0;

class MyWindow : public Gtk::Window {
  Gtk::Combo combo;
  Gtk::Button b;
  Gtk::HBox hbox;
public:
    MyWindow() : b("quit") {
    add (&hbox);
    GList * pList = 0;
    pList = g_list_prepend(pList, "test_item");
    pList = g_list_prepend(pList, "test_item2");
    combo.set_popdown_strings(pList);
    hbox.pack_start(&combo);
    hbox.pack_start(&b);
    b.clicked.connect(Gtk::Main::quit.slot());
    combo.show();
    b.show();
    hbox.show();
    }
    gint delete_event_impl( GdkEventAny *) {
	Gtk::Main::instance()->quit();
    return 0;
    }
};

int main(int argc, char *argv[])
{
    Gtk::Main myapp(&argc, &argv);
    mainwidget=&myapp;
    MyWindow mywin;
    
    
    mywin.set_title("Combo");

    mywin.show();
    
    myapp.run();
    return 0;
}
