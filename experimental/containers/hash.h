/*
 Terraform - (C) RNG
 */


#ifndef _GLIB_HASH_H
#define _GLIB_HASH_H

#include "glib.h"
#include "GlobalSanityCheck.h"


/*
 *  Glib_Hash: very *simple* wrapper for glib/ghash.c
 */
class Glib_Hash
{
public:
                         Glib_Hash (GHashFunc      hash_func,
                                 GCompareFunc      key_compare_func);
                         ~Glib_Hash ();
          void insert         (gpointer key,
                          gpointer value);
          void remove         (gconstpointer key);
          gpointer lookup          (gconstpointer key);
          bool lookup_extended     (gconstpointer lookup_key,
                          gpointer *orig_key,
                          gpointer *value);
          void freeze         ();
          void thaw      ();
          int foreach         (GHFunc    func,
                          gpointer user_data);
          gint foreach_remove      (GHFunc    func,
                          gpointer user_data);
          gint size      ();

private:
          GHashTable          *d_hashTable;
};
inline Glib_Hash::Glib_Hash (GHashFunc hash_func, GCompareFunc key_compare_func)
{
  SanityCheck::bailout ((!hash_func), "hash_function == NULL",
"GlibHash::GlibHash");
  SanityCheck::bailout ((!key_compare_func), "key_compare_function == NULL",
"GlibHash::GlibHash");

     d_hashTable = g_hash_table_new (hash_func, key_compare_func);
}


inline Glib_Hash::~Glib_Hash ()
{
     if (d_hashTable)
          g_hash_table_destroy (d_hashTable);
}


inline void Glib_Hash::insert (gpointer key, gpointer value)
{
     g_hash_table_insert (d_hashTable, key, value);
}


inline void Glib_Hash::remove (gconstpointer key)
{
        g_hash_table_remove (d_hashTable, key);
}
inline gpointer Glib_Hash::lookup (gconstpointer key)
{
        return g_hash_table_lookup (d_hashTable, key);
}


inline bool Glib_Hash::lookup_extended (gconstpointer   lookup_key,
                         gpointer  *orig_key,
                         gpointer  *value)
{
        return g_hash_table_lookup_extended (d_hashTable, lookup_key,
                              orig_key, value);
}


inline void Glib_Hash::freeze ()
{
        return g_hash_table_freeze (d_hashTable);
}
inline void Glib_Hash::thaw ()
{
        return g_hash_table_thaw (d_hashTable);
}


// slightly different sig from orig: C++ objects to GHRFunc being used
inline gint Glib_Hash::foreach (GHFunc        func,
                    gpointer  user_data)
{
        g_hash_table_foreach (d_hashTable, func, user_data);
     return (0);
}


// slightly different sig from orig: C++ objects to GHRFunc being used
inline gint Glib_Hash::foreach_remove (GHFunc  func,
                                       gpointer user_data)
{
        g_hash_table_foreach (d_hashTable, func, user_data);
     return (0);
}
inline gint Glib_Hash::size ()
{
     return g_hash_table_size (d_hashTable);
}

#endif // GLIB_HASH_H
