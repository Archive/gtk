<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/xhtml1-transitional.dtd">
divert(-1)

###
### Set the quote to something better
###
changequote([[, ]])
changecom()

###
###
define([[GTK_VERSION_STABLE]],[[1.2.10]])dnl
define([[GTK_VERSION_DEVEL]],[[1.3.15]])dnl
define([[GNOME_VERSION_STABLE]],[[1.2.2]])dnl
define([[GNOME_VERSION_DEVEL]],[[ 1.3.5 ]])dnl

define([[LOWER]],[[translit([[$*]],[[ABCDEFGHIJKLMNOPQRSTUVWXYZ]],[[abcdefghijklmnopqrstuvwxyz]])]])
define([[NAMIZE]],[[translit(LOWER([[$*]]),[[- ?+]],[[M_QP]])]])
define([[MENUNAMIZE]],[[translit(LOWER([[$*]]),[[- ?+]],[[-_QP]])]])


################################################################
### TOP()
###
define([[TOP]],[[dnl
dnl gtkmm Header 
<a href="index.html">dnl
<img src="pix/gtkmm_logo.gif" alt="gtkmm" border="0" width="10%" height="10%" />dnl
</a>dnl
 <img src="pix/top.gif" alt="gtkmm" border="0" width="400" height="40" />dnl
]])


################################################################
### MENUTEM(item name,link,key,active,indent)
###
define([[MENU_ITEM]],[[dnl
<tr><td nowrap="nowrap"><img src="pix/blank.gif" border="0" width="$5" height="4" alt=" " />dnl
<img src="pix/[[]]ifelse($3,$4,,in)[[]]active.gif" alt="ifelse($3,$4, ,*)" />dnl
<a href="$2"><font size="-1"><b>$1</b></font></a></td></tr>dnl
]])


#####################################################
### MENU(active item)
###
define([[MENU]],[[dnl
  <table border="0" align="left" width="150">
    MENU_ITEM(gtkmm,index.html,main,$1,4)
    MENU_ITEM(Features,features.html,features,$1,24)
    MENU_ITEM(License,license.html,license,$1,24)
    MENU_ITEM(Philosophy,philosophy.html,philosophy,$1,24)
    MENU_ITEM(Requirements,requirements.html,requirements,$1,24)
    MENU_ITEM(Mailinglist,mailinglist.html,mailinglist,$1,24)
    MENU_ITEM(Developers,developers.html,developers,$1,24)
    MENU_ITEM(Documentation,documentation.html,documentation,$1,4)
    MENU_ITEM(Examples,[[example.html]],examples,$1,24)
    MENU_ITEM(Tutorial,[[tutorial/t1.html]],tutorials,$1,24)
    MENU_ITEM(Reference,[[documentation.html#reference]],reference,$1,24)
    MENU_ITEM(Other,[[documentation.html#other]],other,$1,24)
    MENU_ITEM(Stable,stable.html,stable,$1,4)
    MENU_ITEM(Download,download.html,download,$1,24)
    MENU_ITEM(Release Notes,release_notes.html,notes,$1,24)
    MENU_ITEM(Bugs,bugs.html,bugs,$1,24)
    MENU_ITEM(CVS,cvs.html,cvs,$1,24)
    MENU_ITEM(Extra,extra.html,extra,$1,4)
    MENU_ITEM(Links,[[extra.html#links]],links,$1,24)
  </table>
]])


#####################################################
### HEADER(page title, menu active item)
###
define([[HEADER]],[[dnl
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title> gtkmm: $1 </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body bgcolor="#F0F0F0" link="#0000C0" vlink="#000060" >
<table align="center" width="90%" border="0">
<tr>
<td valign="top">
TOP()dnl
</td>
<td rowspan="2" valign="top"> 
<img src="pix/vdiv.gif" align="left" alt="|" border="0" width="8" height="320" />
MENU($2)dnl
</td>
</tr>
<tr>
<td valign="top">
<h2>$1</h2>
]])

define([[LINK_SF]],[[<a href="http://sourceforge.net/$1?group_id=1161">$2</a>]])
define([[ICON]],[[<img src="pix/$1.gif" border="0" width="24" height="24" alt="$2" />]])

#####################################################
### NEWSECTION()
###
define([[NEWSECTION]],[[dnl
</td></tr>
<tr><td colspan="2">
]])

#####################################################
### FOOTER()
###
define([[FOOTER]],[[dnl
</td></tr><tr>
<td colspan="2">
<div align="right">
<table>
<tr>
<td>LINK_SF(project/,ICON(anvil24,Record))</td>
<td><a href="http://gtkmm.sourceforge.net">ICON(home,Home)</a></td>
<td>LINK_SF(bugs/,ICON(bug,Bugs))</td>
<td>LINK_SF(mail/,ICON(mail,Mailing List))</td>
<td>LINK_SF(news/,ICON(news,News))</td>
<td>LINK_SF(project/filelist.php,ICON(save,Download))</td>
</tr>
</table>
</div>
<div align="center"><hr align="center" noshade="noshade"></hr></div>
<table width="100%" border="0">
<tr><td>
<address>
ADDR(gtkmm development team mailing list, gtkmm-main@lists.sourceforge.net)
</address>
Last updated: <i>esyscmd([[ date +"%A, %B %d, %Y"]])</i>
</td>
<td align="right">
<a href="http://sourceforge.net"> 
<img src="http://sourceforge.net/sflogo.php?group_id=1161&amp;type=1" alt="*SOURCEFORGE*" width="88" height="31" border="0" /></a> 
dnl ifelse($1,[[]],[[]],dnl
<a href="http://validator.w3.org/check/referer"><img
src="http://validator.w3.org/images/vxhtml10"
alt="Valid XHTML 1.0!" height="31" width="88" border="0" /></a>
dnl )dnl
</td>
</tr>
</table>
</td></tr>
</table>
</body>
</html>
]])

###
### SUBPAGE(text,name)
###
define([[SUBPAGE]],[[<a name="$2"></a><h2>$1</h2>]])

###
### SECTION(text,name)
###
define([[SECTION]],[[<a name="$2"><font size="+1"><b>$1</b></font></a><br></br>]])


###
### Address macro
###
define([[ADDR]],[[dnl
<a href="mailto:$2">$1</a>
]])dnl



###
### Web Addresses
###
define([[REF_GTK]],[[<a href="http://www.gtk.org">$1</a>]])dnl
define([[REF_GIMP]],[[<a href="http://www.gimp.org">$1</a>]])dnl
define([[REF_GNOME]],[[<a href="http://www.gnome.org">$1</a>]])dnl
define([[REF_GNOME_DIST]],[[<a href="http://www.gnome.org/start/gnometar-new.phtml">$1</a>]])dnl
define([[REF_GDEV]],[[<a href="http://www.gdev.org">$1</a>]])dnl
define([[REF_GPL]],[[<a href="http://www.gnu.org/copyleft/gpl.html">$1</a>]])dnl
define([[REF_LGPL]],[[<a href="license.html">$1</a>]])dnl
define([[REF_DEB]],[[<a href="http://www.debian.org">$1</a>]])dnl
define([[REF_BASE]],[[<a href="http://gtkmm.sourceforge.net/$2">$1</a>]])dnl
define([[REF_OLDBASE]],[[<a href="http://lazy.ton.tut.fi/gtk--/$2">$1</a>]])dnl
dnl
define([[REF_CVS]],[[<a href="http://www.cvshome.org/">$1</a>]])dnl
define([[REF_CVSINTRO]],[[<a href="http://www.cvshome.org/cyclic/cvs/doc-blandy-text.html">$1</a>]])dnl
define([[REF_CVSMAN]],[[<a href="http://www.loria.fr/~molli/cvs/doc/cvs_toc.html">$1</a>]])dnl
define([[REF_HVR]],[[<a href="http://www.hvrlab.org/pub/gtk--/$2">$1</a>]])dnl
define([[REF_SIGC]],[[<a href="http://libsigc.sourceforge.net">$1</a>]])dnl
define([[REF_FAQ]],[[<a href="http://gtkmm.sourceforge.net/docs/gtkmm-faq.html">$1</a>]])dnl


define([[__tlat_cmds__]],[['dnl
s/&/\&amp;/g
s/>/\&gt;/g
s/</\&lt;/g
s//<\/pre><hr \/><pre>/g
']])

define([[preinclude]],[[<pre>syscmd(sed __tlat_cmds__ $1)</pre>]])

divert(0)dnl
