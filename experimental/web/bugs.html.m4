include(base.m4)

HEADER(Bugs,bugs)

SECTION(Current bugs/patches,currentbugs)
<ul>
<li>Outstanding <a href="http://bugzilla.gnome.org/buglist.cgi?product=gtkmm&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=NEEDINFO&bug_status=REOPENED&bug_status=VERIFIED&email1=&emailtype1=substring&emailassigned_to1=1&email2=&emailtype2=substring&emailreporter2=1&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&short_desc=&short_desc_type=substring&long_desc=&long_desc_type=substring&bug_file_loc=&bug_file_loc_type=substring&status_whiteboard=&status_whiteboard_type=substring&keywords=&keywords_type=anywords&op_sys_details=&op_sys_details_type=substring&version_details=&version_details_type=substring&cmdtype=doit&namedcmd=gnome-libs+past+20+days&newqueryname=&order=Reuse+same+sort+as+last+time&form_name=query">gtkmm bugs</a>
(<a href="http://bugzilla.gnome.org/buglist.cgi?product=gtkmm&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=NEEDINFO&bug_status=REOPENED&bug_status=VERIFIED&email1=&emailtype1=substring&emailassigned_to1=1&email2=&emailtype2=substring&emailreporter2=1&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&short_desc=&short_desc_type=substring&long_desc=&long_desc_type=substring&bug_file_loc=&bug_file_loc_type=substring&status_whiteboard=&status_whiteboard_type=substring&keywords=PATCH&keywords_type=anywords&op_sys_details=&op_sys_details_type=substring&version_details=&version_details_type=substring&cmdtype=doit&namedcmd=gnome-libs+past+20+days&newqueryname=&order=Reuse+same+sort+as+last+time&form_name=query">patches</a>).</li>
<li>Outstanding <a href="http://bugzilla.gnome.org/buglist.cgi?product=gnomemm&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=NEEDINFO&bug_status=REOPENED&bug_status=VERIFIED&email1=&emailtype1=substring&emailassigned_to1=1&email2=&emailtype2=substring&emailreporter2=1&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&short_desc=&short_desc_type=substring&long_desc=&long_desc_type=substring&bug_file_loc=&bug_file_loc_type=substring&status_whiteboard=&status_whiteboard_type=substring&keywords=&keywords_type=anywords&op_sys_details=&op_sys_details_type=substring&version_details=&version_details_type=substring&cmdtype=doit&namedcmd=gnome-libs+past+20+days&newqueryname=&order=Reuse+same+sort+as+last+time&form_name=query">gnomemm bugs</a>
(<a href="http://bugzilla.gnome.org/buglist.cgi?product=gnomemm&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=NEEDINFO&bug_status=REOPENED&bug_status=VERIFIED&email1=&emailtype1=substring&emailassigned_to1=1&email2=&emailtype2=substring&emailreporter2=1&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&short_desc=&short_desc_type=substring&long_desc=&long_desc_type=substring&bug_file_loc=&bug_file_loc_type=substring&status_whiteboard=&status_whiteboard_type=substring&keywords=PATCH&keywords_type=anywords&op_sys_details=&op_sys_details_type=substring&version_details=&version_details_type=substring&cmdtype=doit&namedcmd=gnome-libs+past+20+days&newqueryname=&order=Reuse+same+sort+as+last+time&form_name=query">patches</a>).</li>
<li><a href="http://bugzilla.gnome.org/buglist.cgi?product=gnomemm&product=gtkmm&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=NEEDINFO&bug_status=REOPENED&bug_status=VERIFIED&email1=&emailtype1=substring&emailassigned_to1=1&email2=&emailtype2=substring&emailreporter2=1&changedin=&chfieldfrom=&chfieldto=Now&chfieldvalue=&short_desc=&short_desc_type=substring&long_desc=&long_desc_type=substring&bug_file_loc=&bug_file_loc_type=substring&status_whiteboard=&status_whiteboard_type=substring&keywords=easy-fix&keywords_type=anywords&op_sys_details=&op_sys_details_type=substring&version_details=&version_details_type=substring&cmdtype=doit&namedcmd=gnome-libs+past+20+days&newqueryname=&order=Reuse+same+sort+as+last+time&form_name=query">easy-fix bugs</a> - a good way to get involved.</li>
</ul>

SECTION(Where to report?,report)
<ul>
<li>REF_BASE(ICON(mail,Mailling List) gtkmm mailing list,mailinglist.html) </li>
<li>Bugs are tracked in GNOME's <a href="http://bugzilla.gnome.org">bugzilla</a><br>
Submit gtkmm bugs <a href="http://bugzilla.gnome.org/enter_bug.cgi?product=gtkmm">here</a>.<br>
Submit gnomemm bugs <a href="http://bugzilla.gnome.org/enter_bug.cgi?product=gnomemm">here</a>.
</li>
<li>Patches should be attached to bugzilla bugs rather than sent to the mailing list. Please use the PATCH keyword in bugzilla.</li>
</ul>
<p />

SECTION(What information is needed for a bug report?,info)
<ul>
<li>version numbers of GTK+, gtkmm and other related software</li>
<li>description of the bug</li>
<li>information to reproduce the bug</li>
<li>stack dump, if relevant:
<pre>
gdb ./proggy ; run ; *crash* ; where
</pre></li>
<li>keep it small but detailed</li>
<li>if possible, provide a patch</li>
</ul>

SECTION(How do I create a patch?,patch)
<ul>
<li>Take code from cvs</li>
<li>modify that version</li>
<li>check it compiles</li>
<li>use cvs to build a patch
<pre>
  cvs diff -u &gt; my_fix.patch
</pre></li>
<li>Add the patch to bugzilla (see above).</li>
</ul>

SECTION(Should I send a bug report?,when)
<ul>
<li>Always send a bug report:
<ul>
  <li>If you get sigsegv and you think it might be problem with gtkmm.</li>
  <li>If you cannot compile gtkmm.</li>
  <li>If you have a good idea of how to make gtkmm better.</li>
</ul>
</li>
<li>Never send a bug report:
<ul>
  <li>If release notes or this web page mentions the bug. (a patch would be neat though)</li>
</ul></li>
</ul>


FOOTER()


