#!/usr/bin/perl

# Stupid example highlighter
# Copyright 1999, Karl Nelson

# this cheats rather badly to make a decent looking 
# syntax analysis.  It uses our conventions to figure out
# stuff it doesn't know as well as predefined hints on 
# what words could mean.  Not nearly 100% but close enough.

print "
<html>
<head>
<title> Gtk--: Documentation </title>
</head>
<base href=\"http://gtkmm.sourceforge.net\">
<body bgcolor=#E8E8E8>
<pre>
";

$debug=0;

#---------------------------------------------------------------------
$specifier{"const"}=1;
$specifier{"static"}=1;
$specifier{"virtual"}=1;

$keyword{"switch"}=1;
$keyword{"case"}=1;
$keyword{"default"}=1;
$keyword{"break"}=1;
$keyword{"new"}=1;
$keyword{"delete"}=1;
$keyword{"if"}=1;
$keyword{"else"}=1;
$keyword{"return"}=1;
$keyword{"true"}=1;
$keyword{"false"}=1;
$keyword{"for"}=1;
$keyword{"template"}=1;
$keyword{"public"}=1;
$keyword{"private"}=1;
$keyword{"protected"}=1;
$keyword{"class"}=1;
$keyword{"struct"}=1;
$keyword{"this"}=1;
$keyword{"while"}=1;
$keyword{"using"}=1;
$keyword{"namespace"}=1;
$keyword{"dynamic_cast"}=1;
$keyword{"const_cast"}=1;

#----------------------------------------------------------------

$enum{"NULL"}=1;
$enum{"TRUE"}=1;
$enum{"FALSE"}=1;

$function{"main"}=1;
$function{"bind"}=1;

$sig{"quit"}=1;
$sig{"idle"}=1;
$sig{"show"}=1;
$sig{"hide"}=1;
$sig{"toggled"}=1;
$sig{"clicked"}=1;

$var{"cout"}=1;
$var{"cin"}=1;
$var{"cerr"}=1;

$class{"FILE"}=1;
$class{"list"}=1;
$class{"string"}=1;
$class{"ifstream"}=1;
$class{"file"}=1;
$class{"vector"}=1;
$class{"list"}=1;
$class{"map"}=1;
$class{"Slot0"}=1;
$class{"Slot1"}=1;
$class{"Slot2"}=1;
$class{"Signal0"}=1;
$class{"Signal1"}=1;
$class{"Signal2"}=1;
$class{"Connection"}=1;
$class{"iterator"}=1;

$namespace{"Gtk"}=1;
$namespace{"Gnome"}=1;

$newline=1;

#suck up the buffer
while(<>)
  {$line.=$_;}

# Parse the output to learn the vocabulary
$line4=$line;
$line4=~s/#include.*/ /g;
$line4=~s/\/\/.*/ /g;
$line4=~s/\s+/ /g;
$line4=~s/([A-Za-z0-9_]) ([^A-Za-z0-9_])/$1$2/g;
$line4=~s/([^A-Za-z0-9_]) /$1/g;
$line5=$line4;
#learn all signals from use
while($line5=~s/([A-Za-z0-9_]+)\.connect// && $1)
  {
    $sig{$1}=1;
  }
while($line5=~s/([A-Za-z0-9_]+)\.slot// && $1)
  {
    $sig{$1}=1;
  }
while($line4=~s/^(([A-Za-z0-9_]+)|(\/\*)|(\/\/)|.|\s+|\n)//)
  {
   $token=$1;
   next if ( $token =~ /^[0-9]+$/);
   next if ( $token eq " ");

   # C comments
   if ($token eq "/*")
     {
      while ($line4=~s/^((\*\/)|\n|.)//)
        {
         last if ($1 eq "*/");
        }
      next;
     }

   # strings
   if ($token eq "\"")
     {
      $line4=~s/^([^\"]*\")//;
      next;
     }

   next if ( $token =~ /^[^A-Za-z0-9_]/);

   # learn new class name
   if ($token =~ /^((class)|(struct))$/ )
     {
       $line4 =~ /^\s+([A-z0-9_]+)/;
       $token2=$1;
       if ($token2 && !&isknown($token2))
         {
           $class{$token2}=1;
           print "Class: $token2\n" if ($debug);
         }
       next;
     }

   # learn new variable name
   if ( &istype($token))
     {
       $line4=~/([^(;]+[;(])/;
       $line3=$1;
       $line3=~s/^<[^>]*>//;
       next if ($line3 =~ /^,/);
       while ($line3 =~ s/^[\n *&,]+([A-Za-z0-9_]+)// && $1)
         {
           $token2=$1;
           last if ( &istype($token2) || &isfunction($token2) );
           if ($line3 =~ /^\s*\(/ )
             { 
               $function{$token2}=1;
               print "Func: $token\n" if ($debug);
               last;
             }
           $var{$token2}=1;
           print "Var: $token $token2\n" if ($debug);
         };
       next;
     };

   next if (&isknown($token));

   if ($token =~ /^([A-Z]+[a-z]+)+$/)
     {
       $class{$token}=1;
       print "Class: $token\n" if ($debug);
       $line4="$token$line4";
       next; 
     }

   if ( $line4 =~ /^\(/ )
     {
       $function{$token}=1;
       print "Func: $token\n" if ($debug);
     };

  }

# Print the output
while($line=~s/^(([A-Za-z0-9_]+)|(\/\*)|(\/\/)|.|\s+|\n)//)
  {
   $token=$1;

   if ($newline && $token eq "#")
     {
      $line=~s/^(.*\n)//;
      print "<font color=darkgreen>";
      print &html("#$1");
      print "</font>";
      next;
     }

   $newline=0;
   $newline=1 if ($token =~ /\n/ );

   # C comments
   if ($token eq "/*")
     {
      $line3="";
      while ($line=~s/^((\*\/)|\n|.)//)
        {$line3.="$1";
         last if ($1 eq "*/");
        }
      print "<font color=darkred><i>";
      print &html("/*$line3");
      print "</i></font>";
      next;
     }
  
   # C++ comments
   if ($token eq "//")
     {
      $line=~s/^(.*\n)//;
      print "<font color=maroon><i>";
      print &html("//$1");
      print "</i></font>";
      $newline=1;
      next;
     }

   # strings
   if ($token eq "\"")
     {
      $line=~s/^([^\"]*\")//;
      print "<font color=green><i>";
      print &html("\"$1");
      print "</i></font>";
      next;
     }

   if ($token =~ /^[^A-Za-z0-9]+/)
     {
      print &html($token);
      next;
     }

   # keywords
   if ( &iskeyword($token) )
     {
      print "<font color=black><b>$token</b></font>";
      next;
     }

   #functions
   if ( &isfunction($token) )
     {
      print "<font color=blue><b>$token</b></font>";
      next;
     }

   # ENUMS
   if ( &isenum($token) )
     {
      print "<font color=green>$token</font>";
      next;
     }

   # types
   if ( &isintrensictype($token) )
     {
      print "<font color=purple>$token</font>";
      next;
     }

   #class names
   if ( &isnamespace($token) )
     {
      print "<font color=navy><b>$token</b></font>";
      next;
     }

   if ( &isclasstype($token) )
     {
      print "<font color=navy><b>$token</b></font>";
      next;
     }

   #emun names
   if ( &isenumtype($token) )
     {
      print "<font color=navy>$token</font>";
      next;
     }

   if ( &issig($token) )
     {
      print "<font color=teal>$token</font>";
      next;
     }

   if ( &isvar($token) )
     {
      print "<font color=blue>$token</font>";
      next;
     }

   if ( &isspecifier($token) )
     {
      print "$token";
      next;
     }
   
   #guess
   if ( $token=~/([A-Z]+[a-z]+)+/)
     {
      print "<font color=navy><b>$token</b></font>";
      next;
     }

   print &html($token);
  }

print "
</pre>
</html>
";

#-----------------------------------------------------------------------
sub html
  { 
    local($str)=@_;
    $str=~s/&/&amp;/g; 
    $str=~s/</&lt;/g; 
    $str=~s/>/&gt;/g; 
    $str;
  }

sub isknown
  {
    local($str)=@_;
    ( &iskeyword($str) || &isspecifier($str) || &isintrensictype($str) || 
      &isfunction($str) || &isclasstype($str) || &isvar($str) || &issig($str) ||
      &isenumtype($str) || &isenum($str) || &isnamespace($str) );
  }

sub istype
  {
    local($str)=@_;
    ( &isintrensictype($str) || &isclasstype($str) || isenumtype($str));
  }

sub isspecifier
  {
    local($str)=@_;
    ( $specifier{$str} );
  }

sub isnamespace
  {
    local($str)=@_;
    ( $namespace{$str} );
  }

sub isintrensictype
  {
    local($str)=@_;
    ( $str =~ /^(g*)((void)|(char)|(int)|(float)|(double)|(bool)|(boolean))$/ );
  }

sub isfunction
  {
    local($str)=@_;
  #  ($str =~ /^((gtk_.*)|(gdk_.*)|(g_.*)|(get_.*)|(set_.*)|(.*_impl)|(pack_.*)|(append_.*))$/ || $function{$str});
    ($function{$str} || $str=~ /^((.*_impl))$/);
  }

sub isclasstype
  {
    local($str)=@_;
    ($class{$str} || $str =~ /^((Gdk_.*)|(.*List)|(.*Elem))$/);
  }

sub isvar
  {
    local($str)=@_;
    ($var{$str});
  }

sub issig
  {
    local($str)=@_;
    ($sig{$str} || $str =~ /^((.*_event))$/);
  }

sub iskeyword
  {
    local($str)=@_;
    ($keyword{$str});
  }

sub isenumtype
  {
    local($str)=@_;
    ($str =~ /^((Gtk.*)|(Gdk.*))$/);
  }

sub isenum
  {
    local($str)=@_;
    ($str =~ /^((GTK.*)|(GDK.*))$/ || $enum{$str} );
  }
