include(base.m4)
HEADER(CVS,cvs)
CVS is a powerful tool for maintain version control with
multiple developers produced by REF_CVS(Cyclic).  
Both the stable and developer versions
of gtkmm are housed in the REF_GNOME(Gnome) CVS repository.  
<p />
Documentation for CVS is available at 
REF_CVSINTRO(Introduction to CVS) or the REF_CVSMAN(CVS manual).
CVS is available on a wide number of
platforms including Unix, Window 95/NT, and Macintosh.  
<p />
<br />
SECTION(Anonymous CVS,anonymous)
<br />
The main CVS repository is located at cvs.gnome.org and requires
a developer login.
For those who do not have a login on the gnome developer tree,
anonymous CVS checkouts are available from anoncvs.gnome.org.
<p />
</td></tr>
<tr><td colspan="2">
To check out the latest development version:
<pre>
cvs -d :pserver:anonymous@anoncvs.gnome.org:/cvs/gnome login 
<i>(hit return as the password)</i>
cvs -d :pserver:anonymous@anoncvs.gnome.org:/cvs/gnome co gtkmm-1.3
</pre>
<p />
The stable version (with any bug fixes made between releases) is
also available.
<p />
<pre>
cvs -d :pserver:anonymous@anoncvs.gnome.org:/cvs/gnome login 
<i>(hit return as the password)</i>
cvs -d :pserver:anonymous@anoncvs.gnome.org:/cvs/gnome co gtk--
</pre>
<p />
<br />
<p>You will need to run the autogen.sh script to generate the build files.</p>
<p />

FOOTER()
