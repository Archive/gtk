include(base.m4)
HEADER(Requirements,requirements)

SECTION(Requirements,require)
<ul>
<li>REF_GTK(GTK+)</li>
<li>REF_SIGC(libsigc++)</li>
<li>REF_GNOME(gnome-libs) (optional)</li>
</ul>

SECTION(GTK+,gtk)
<p>You can download GTK+ and GNOME from <a href="http://download.gnome.org">download.gnome.org</a>.</p>


SECTION(Tools,tools)
The following tools are required for building gtkmm source packages: 
<ul>
  <li>GNU m4 (<a href="ftp://ftp.funet.fi/pub/gnu/gnu/">get</a>) </li>
  <li>GNU make (<a href="ftp://ftp.funet.fi/pub/gnu/gnu/">get</a>)</li>
  <li>Perl</li>
</ul>


FOOTER()
