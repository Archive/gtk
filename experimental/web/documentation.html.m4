include(base.m4)
HEADER(Documentation,documentation)

SUBPAGE(gtkmm2 Documentation,gtkmm2)
<p><a href="/gtkmm2">Here</a> is an overview of the gtkmm2 documentation. This is the focus of most of our efforts at the moment.</p>

SECTION(gtkmm/GTK+ documentation,documentation)
Sources of information for using gtkmm
<ul>
<li>REF_FAQ(FAQ)</li>
<li><a href="http://www.gtk.org/rdp/">GTK+ documentation project</a></li>
</ul>

SUBPAGE(Examples &amp; Tutorials,tutorials)

SECTION(gtkmm tutorial,gtkmm_tutorial)
An early draft of the
REF_BASE(gtkmm tutorial,tutorial/t1.html) is now available.
If you're a newcomer to GTK+/gtkmm, start here. Check back often for updates.
<p />
You can get the tutorial in many ways
<ul>
<li>Browse in HTML REF_BASE(online,tutorial/t1.html)</li>
<li>Download in REF_BASE(dvi,tutorial/gtkmm-tut.dvi) format</li>
<li>Download in REF_BASE(ps,tutorial/gtkmm-tut.ps) format</li>
<li>Download in REF_BASE(pdf,tutorial/gtkmm-tut.pdf) format</li>
<li>Download the whole HTML as a REF_BASE(tarball,tutorial/gtkmm-tut-html.tar.gz)</li>
</ul>
<p />
We welcome any contributions to the tutorial.  The tutorial
is written in 
<a href="http://gtkmm.sourceforge.net/tutorial/sgml_guide.pdf">sgml</a>.  
Simply checkout a copy of gtkmm-tut.sgml from the gtkmm-1.3 module in anonymous
gnome cvs and make changes.  Once done go to the top level and run a
"cvs update", resolve any conflicts, and then "cvs diff -u &gt; mypatch".
Add the patch to the <a href="http://sourceforge.net/projects/gtkmm/">sourceforge</a>  patch manager and we will have it integrated within the week.
<p />

SECTION(Widgets,widgets)
<ul>
<li>REF_BASE(gtkmm examples, example.html)</li>
<li>These examples are also in the gtkmm distribution.</li>
<li>The gnomemm distribution contains examples too.</li>
</ul>

SECTION(Signal system,signals)
See REF_SIGC(libsigc++)
<p />

SUBPAGE(Reference,reference)
SECTION(Reference manual,manual) 
<br />
<ul>
<li>HTML: <a href="docs/gdk/class_index.html">gdkmm reference</a> 
(<a href="docs/gdk/gdkmm-ref.tar.gz">gdkmm-ref.tar.gz</a>) </li>
<li>HTML: <a href="docs/gtk/class_index.html">gtkmm reference</a>
(<a href="docs/gtk/gtkmm-ref.tar.gz">gtkmm-ref.tar.gz</a>) </li>
<li>HTML: <a href="docs/gnome/class_index.html">gnomemm reference</a>
dnl<li>TAR: <a href="http://lazy.ton.tut.fi/gtk--/refdoc.tar.gz">tar package</a>
</li>
</ul>

SECTION(Design,design)
<ul>
<li>REF_OLDBASE(article,signals.html) about design of the signal system. 
<br />
it doesn't have everything exactly like gtkmm has, but read if you want to modify gtkmm or for deep understanding of gtkmm's signals</li>
</ul>

SUBPAGE(Other,other)

SECTION(Screenshots,screenshots)
<ul>
<li><a href="http://www.gnome.org/screenshots/index.shtml">gnome desktop screenshots</a></li>
</ul>

<br />
FOOTER()
