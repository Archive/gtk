include(base.m4)
HEADER(Stable Version,stable,stable)

SECTION(Stable,stable)
<br />
Latest stable version is: GTK_VERSION_STABLE 
<p />
Downloads are hosted at the sourceforge
LINK_SF(project/filelist.php,download page ICON(save,Downloads)).
<p />
This version is known to work with
<ul>
<li>The latest stable GTK+</li>
<li>gcc 2.95, gcc 2.96 (RedHat), gcc 3</li>
<li>The latest stable REF_SIGC(libsigc++)</li>
</ul>

FOOTER()

