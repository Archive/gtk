include(base.m4)
HEADER(Reference,reference,reference)

SECTION(Reference manual)
<br>
<ul>
<li>HTML: <a href="http://lazy.ton.tut.fi/gtk--docs/">reference</a>
<li>TAR: <a href="http://lazy.ton.tut.fi/gtk--/refdoc.tar.gz">tar package</a>
</ul>

SECTION(Design)
<ul>
<li>REF_BASE(article,signals.html) about design of the signal system. <br>it doesn't have everything exactly like gtkmm has, but read if you want to modify gtkmm or for deep understanding of gtkmm's signals
</ul>

SECTION(GTK+ Documentation)
<ul>
<li><a href="http://www.gtk.org/rdp/">GTK+ documentation project</a>
</ul>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
FOOTER()
