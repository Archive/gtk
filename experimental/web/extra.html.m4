include(base.m4)
HEADER(Extra stuff,extra,extra)

SECTION(Available widgets,widgets)
In addition to the wrappers distributed with gtkmm, additional
widgets are available from other sources.
<ul>
<li>See <a href="http://developer.gnome.org/tools/repo/repo-gtk.html">GTK+ widget repository</a></li>
<li><a href="http://www.ece.ucdavis.edu/~kenelson/gtkglareamm/">GtkGLArea--</a> an OpenGL widget that works with gtkmm</li>
<li><a href="http://cactus.rulez.org/projects/panelmm">Panel--</a>, GNOME panel applet wrapper by Gerg� �rdi</li>
<li><a href="http://gtkextramm.sourceforge.net">GtkExtra--</a>, Wrappers for GtkExtra (sheet, etc)</li>
<li><a href="http://gconfmm.sourceforge.net">GConf--</a>, Wrappers for GConf, particularly GConfClient</li>
dnl<li><a href="http://home.wtal.de/petig/">TCList</a> widget by Christof Petig.</li>
dnl<li><a href="http://www.emccta.com/~trow/gtk.html">canvas and plot widgets</a></li>
</ul>

SECTION(gtkmm tools,tools)
<ul>
<li><a href="http://home.wtal.de/petig/Gtk/">Glade--</a> - Module for <a href="http://glade.pn.org">Glade</a> the GTK+ GUI builder</li>
<li><a href="http://www.geocities.com/SiliconValley/Bit/8083/gladecc.htm">Gladecc</a> Another module for Glade</li>
<li><a href="http://bakery.sourceforge.net/">Bakery</a>- Document/View Model with XML</li>
</ul>
<br />

SECTION(Applications using gtkmm,applications)
If all else fails, sometimes it is good to look at the source of
some other projects using the same toolkit.
(1.2)
<ul>
<li><a href="http://ardour.sourceforge.net/">ardour</a>, a multichannel hard disk recorder and digital audio workstation</li>
<li><a href="http://binars.sourceforge.net/">BINARS</a>, music sequencer for Linux, ALSA, and GNOME</li>
<li><a href="http://gabber.sourceforge.net/">gabber</a>, GNOME Jabber client.</li>
<li><a href="http://cdrdao.sourceforge.net/">gcdmaster</a>, a cdrdao frontend.</li>
<li><a href="http://genSQL.sourceforge.net">genSQL</a>, a generic SQL GUI.</li>
<li><a href="http://nagya.rulez.org/gnomoku/">Gnomoku</a>, Gomoku game for GNOME</li>
<li><a href="http://gnuvoice.sourceforge.net">gnuvoice</a>, a GUI voicemail / speakerphone / caller ID application.</li>
<li><a href="http://gtkmail.sourceforge.net">gtkmail</a>, a MIME compliant GUI email client</li>
<li><a href="http://cactus.rulez.org/projects/guikachu">Guikachu</a>, graphical resource editor for PalmOS applications</li>
<li><a href="http://www.akio-solutions.com/guillaume/">Gvier</a>, game based off "Power of four"</li>
<li><a href="http://gvbox.sourceforge.net/">Gvbox</a>, graphical frontend to isdn4linux answering machine</li>
<li><a href="http://yotam.freehosting.net/software/ped/ped.html">Ped</a> Path editor utility </li>
<li><a href="http://PicMonger.sourceforge.net">PicMonger</a> Usenet picture grabber.</li>
<li><a href="http://cactus.rulez.org/projects/radioactive/">RadioActive</a>, radio application for Video4Linux-compatible radio tuner cards.</li>
<li><a href="http://softwerk.sourceforge.net/">softwerk</a>, software analogue MIDI sequencer</li>
<li><a href="http://terraform.sourceforge.net/">terraform</a>, Terrain generator (very cool)</li>
<li><a href="http://www.glom.org/">Glom</a> MySQL Database GUI</li>
<li><a href="http://lostirc.sourceforge.net/">LostIRC</a>, an IRC client</li>
<li><a href="http://ickle.sourceforge.net/">ickle</a>, an ICQ client</li>
<li><a href="http://cap.sourceforge.net/">Chained Audio Plugins</a></li>
<li><a href="http://guillaume.cottenceau.free.fr/html/grany.html">Grany-3</a>, the cellular automaton simulator. </li>
<li><a href="http://paloma.sourceforge.net/">Paloma</a>, music management system</li>
<li><a href="http://www.quasimodo.org/">Quasimodo</a> Real-time audio/MIDI Environment (cool)</li>
</ul>
(1.0)
<ul>
<li><a href="http://mage.rulez.org/">Mage adventure game engine</a>, creates playing interactive fiction games.</li>
<li><a href="http://www.all-day-breakfast.com/rosegarden/">Rosegarden</a>, MIDI sequencer and musical notation editor</li>
dnl
dnl
dnl
dnl<li><a href="http://schudel.penguinpowered.com/~dan/MonitorSensors/">MonitorSensors</a>, Graphical interface to <a href="http://www.netroedge.com/~lm78/">Lm_sensors</a>.
dnl<li><a href="http://licq-gtkmm-gui.sourceforge.net">LICQ plugin</a>, Plugin to use LICQ without Qt.
dnl<li><a href="http://dachshund.sourceforge.net">Dachshund</a>, an effort to create a high-quality visual modeling tool supporting UML.
dnl<li><a href="http://www.crosswinds.net/~neogargoyle/heinke/DSA-Tools.html">DSA Tools</a>,  Character generator for German RPG (in German of course).
dnl<li><a href="http://www.ist.org/eos/">EOS</a>, Electronic Object Simulator (dead?)
</ul>
<p />


SUBPAGE(Links,links)
SECTION(GTK+ resources,gtk)
<ul>
<li>Sunsite's <a href="http://www.ibiblio.org/gtk/">GTK+ app repository</a></li>
<li>REF_GIMP(Gimp homepage)</li>
<li>REF_GTK(GTK+ homepage)</li>
<li>REF_GNOME(GNOME homepage)</li>
</ul>
<p />

SECTION(Other widget sets,other)
Okay so maybe gtkmm isn't what you were looking for.  There are a good
number of other C++ widget sets available.
<ul>
<li><a href="http://fltk.easysw.com/">Fltk</a> (LGPL)</li>
<li><a href="http://www.freiburg.linux.de/~wxxt/">wxGTK</a> (LGPL+exception)</li>
<li><a href="http://www.trolltech.com">Trolltech's Qt</a> (QPL)</li>
</ul>

SECTION(Widget resource sites,resources)
<ul>
<li><a href="http://atai.org/guitool/">The GUI Toolkit, Framework Page</a></li>
<li><a href="http://SAL.KachinaTech.COM/F/5/index.shtml"> SAL: X11 Libraries and Toolkits </a></li>
</ul>

SECTION(Listing sites,lists)
This is just a list of sites that point to gtkmm.  It 
should provided to help you find similar libraries and 
related resources.  (somewhat sorted by relevance) 
<ul>
<li> <a href="http://www.freshmeat.net">Freshmeat</a></li>
<li> <a href="http://www.linuxberg.com">Linux Berg</a></li>
<li> <a href="http://www.linuxlinks.com">Linux Links</a></li>
<li> <a href="http://www.mathtools.net">Mathtools.net</a></li>
</ul>

SECTION(Other interesting info,interest)
<ul>
<li><a href="ftp://ftp.lettere.unipd.it/pub/linux/gdk_multi-0.2.tar.gz">gdk_multi-0.2.tar.gz</a> - printing with gtk</li>
<li><a href="http://osiris.sunderland.ac.uk/~cs0her/page1401.html">OO principles</a></li>
<li><a href="http://www.mpoli.fi/flug/x-ohjelmointi.html">Finnish documentation</a> about X11 and GTK+.</li>
<li><a href="http://libsigc.sourceforge.net/"> Libsigc++</a>, the GTK+/gtkmm signal system translated to C++.</li>
</ul>

FOOTER()
