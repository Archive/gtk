include(base.m4)
HEADER(Release notes,development,release)
<!-- @@ -->


<h3>Gtk--1.0.1 (21 May 1999)</h3>
Get <a href="Gtk---1.0.1.tar.gz">Gtk---1.0.1.tar.gz</a>.
<ul>
<li>1.0.1 release test run
<li>A lot of bug fixes.
<li>Do not trust this release yet! Tell me all compilation problems!
</ul>
<hr noshade>

<h3>Patches for 1.0.0</h3>
<ul>
<li>REF_BASE(patch,gtkmm_1_0_patch_itemfactory.txt) for sigsegv in Gtk_ObjectHandle.
<li>REF_BASE(instructions,gtkmm_1_0_patch_irix.txt) for compiling gtkmm-1.0 to irix.
</ul>


<h3>Gtk--1.0.0 ( 6 Mar 1999)</h3>
Get <a href="Gtk---1.0.0.tar.gz">Gtk---1.0.0.tar.gz</a>.
<ul>
<li>1.0 release test run
<li>fixed namespace problems (hope this works)
<li>fixed some goofs from 0.99.1
</ul>
<hr noshade>

FOOTER()












