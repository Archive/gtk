include(base.m4)
HEADER(Statement regarding Inti and GNOME)
<h3>Update</h3>
In addition to the following explanation, you should also know that Inti is not being actively maintained at this time.
<p/>
<i> -- Murray Cumming, 12th January 2002</i>
<p/>
<h3>gtkmm to be distributed with GNOME 1.4</h3>
Contrary to recent rumours, gtkmm is not being phased out as the 
C++ binding for <a href="http://www.gnome.org">GNOME</a>.    
Gtkmm and Gnomemm are the official bindings for
C++ in the GNOME project and will remain so for the foreseeable 
future.  Gtkmm is an official GNOME project and is currently being transfered
to the <a href="http://www.gnu.org">Free Software Foundation</a> 
to become a GNU project.
<p />

Further, the recently started project 
<a href="http://sources.redhat.com/inti">Inti</a> is not the successor 
of the gtkmm project.  Inti
is a separate C++ framework being developed by 
<a href="http://www.redhat.com">Red Hat</a>.  Some of its goals
contradict those long standing <a href="philosophy.html">philosophies</a> 
of gtkmm.  Inti was
written to support 
Red Hat's need for a stable C++ framework for ISV programmers at the 
release of GTK+ 2.0.  It is not part of the GNOME project, though it
does use components in common with GNOME.
It intends to be uncomplicated and thus is not as feature rich of 
a binding as gtkmm,
nor will it include GNOME bindings.  It intends to provide a full foundation 
package providing its own single source solution, contrary to gtkmm's 
philosophy of dependency separation.
It is to have no sharp edges even if that
means altering standard C++ conventions and use of thick constructs.
Development direction is dictated by features needed to support ISVs.
Inti has a tight time frame of completion and thus is not open
to large scale community input.  This pretty much rules out participation
of the current gtkmm developers.  
<a href="mailto:hp@redhat.com">Havoc</a> did not
intend for Inti to target developers within the free software community.
For these reasons and other creative 
differences, gtkmm and Inti are separate projects.  
Both Havoc and I agreed to this fork as the most acceptable 
solution given his requirements.
<p />

Unfortunately, Inti did create a split amongst the gtkmm developers.
Long time maintainer 
<a href="http://www.telegraph-road.org/">Guillaume Laurent</a> 
left the project after a
long and bitter debate, then he endorsed the still unreleased
Inti project and switched to Qt/KDE2.  His arguments have been discussed
in detail on the gtkmm mailing list and been found unpersuasive.
Strict STL compliance and use of composite objects are considered
a valued feature by gtkmm users.  Developers considering
his arguments should understand the issue is more complex
than Guillaume portrayed and thus they should make an evaluation for 
themselves.
Guillaume was a valued member of the gtkmm team and not soon replaced.
I am sorry that we could not see eye to this eye on this issue, but it
certainly wasn't the first time.  Despite his loss, gtkmm will continue.
As always anyone interested in participating in the gtkmm project
is encouraged to contact the 
<a href="mailto:kenelson@users.sourceforge.net">maintainer</a>.
<p />
<i> -- Karl Einar Nelson, March 2001</i>
<p />

FOOTER()
