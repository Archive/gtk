include(base.m4)
HEADER(Supported Features,features)
SECTION(Widget wrapping framework,framework)
<ul>
<li>Automatic wrapping system for converting foreign resources</li>
<li>Access to GTK+ resources for integrating GTK+ tools</li>
<li>Consistent use of reference semantics for parameter passing</li>
<li>OO technologies for combining user interface elements
<ul>
<li>Inheritance </li>
<li>Polymorphic references</li>
<li>Type-safe callbacks</li>
</ul>
</li>
<li>Complete memory management
<ul>
<li>Object composition </li>
<li>Automatic deallocation of dynamically managed memory</li>
</ul>
</li>
</ul>

SECTION(gtkmm widgets,gtk_widgets)
<ul>
<li>Tracks GTK+ development</li>
<li>Single inheritance widget hierarchy</li>
<li>User defined C++ widgets support</li>
<li>Hundreds of C++ wrapper classes</li>
<li>Standard string class support</li>
<li>STL iterator and list support</li>
<li>Factories for constructing Toolbars and Menus</li>
</ul>

SECTION(gnomemm widgets,gnome_widgets)
<ul>
<li> Full Canvas support</li>
<li> Large number of predefined menu items and icons</li>
<li> Preconfigured dialogs </li>
<li> MDI support</li>
</ul>

SECTION(Type-safe signal system,signals) (via sigc++)
<ul>
<li>One-to-Many callbacks</li>
<li>Callbacks to:
<ul>
<li>Member functions</li>
<li>Static member functions</li>
<li>Function objects</li>
<li>Signals</li>
</ul>
</li>
<li>Invalidation of connections when target is deleted</li>
<li>Passing extra data from connect to the callback</li>
<li>Proxies for GTK+ connectivity</li>
</ul>
dnl SECTION(Interfaces supported)
dnl <ul>
dnl <li>GTK+1.0 widgets (71 classes)</li>
dnl <li>GTK+1.2 widgets (13 classes)</li>
dnl <li>Gdk drawing primitives (14 classes)</li>
dnl <li>Gnome widgets (18 classes)</li>
dnl </ul>

FOOTER()
