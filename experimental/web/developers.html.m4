include(base.m4)
HEADER(Developers,developers)
SECTION(Developers and Contributors,developers)
<ul>
<li><b>Current maintainers</b><br />
ADDR(Murray Cumming,murrayc@usa.net) (<a href="http://www.murrayc.com">www.murrayc.com</a>)<br/>
ADDR(Daniel Elstner,daniel.elstner@gmx.net)<br/>
</li>
<li><b>Other gtkmm2 developers</b><br/>
ADDR(Karl Nelson,kenelson@ece.ucdavis.edu)<br/>
ADDR(Gerg&otilde; &Eacute;rdi,cactus@cactus.rulez.org) (<a href="http://cactus.rulez.org">cactus.rulez.org</a>)<br/>
ADDR(Andreas Holzmann,andreas.holzmann@epost.de)<br/>
ADDR(Martin Schulze,mhl.schulze@t-online.de)<br/>
<li><b>Previous maintainers</b><br />
ADDR(Guillaume Laurent,glaurent@telegraph-road.org)<br />
ADDR(Tero Pulkkinen,terop@assari.cc.tut.fi)<br />  
</li>
<li><b>1.2 Documentation Group</b><br />
Michael Ashton, Bernhard Rieder, Robert Gasch, Daniel Schudel
</li>
<li><b>1.2 Major contributors</b><br />
Peter Lerner, Hanee Patenaude, Herbert Valerio Riedel, Todd Dukes, Joe Yandle, Murray Cumming
</li>
<li><b>1.0 Major contributors</b><br />
Elliot Lee, Phil Dawes, Stephan Kulow, Erik Andersen,<br />
Bibek Sahu, Chris Cannam, Mirko Streckenbach, <br />
Havoc Pennington, Marcus Brinkmann, Todd Dukes<br />
</li>
</ul>
There have been many patches, bug fixes and improvements from a lot
of people. Thanks to them.
FOOTER()
