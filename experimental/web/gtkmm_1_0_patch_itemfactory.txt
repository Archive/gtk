diff -u -r1.50.2.2 -r1.50.2.3
--- base.gen_h  1999/03/05 19:20:25     1.50.2.2
+++ base.gen_h  1999/03/12 21:47:25     1.50.2.3
@@ -1,4 +1,4 @@
-/* $Id$ */
+/* $Id$ */

 /* base.h
  *
@@ -378,6 +378,8 @@
   template <class _Tp1> operator Gtk_ObjectHandle<_Tp1>()
     { return Gtk_ObjectHandle<_Tp1>(this->release()); }
 #endif
+  Gtk_ObjectHandle(const Gtk_ObjectHandle &a)
+   : object_(a.release()), owned_(true) {}
   /* a must have ownership to call this, and this object gets
      the ownership */
   Gtk_ObjectHandle& operator=(const Gtk_ObjectHandle &a) {
