include(base.m4)
<html>
<head>
<title>gtkmm - the C++ interface for GTK+</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body bgcolor="#F0F0F0" link="#0000D0" vlink="#000060">
<table align="center" border="0">
<tr><td valign="top">
TOP[[]]dnl
</td>
<td rowspan="2" height="1" width="158" valign="top">
<img src="pix/vdiv.gif" align="left" border="0" width="8" height="300" alt=""></img>
MENU(main)dnl
</td></tr>

<tr><td valign="top">
  <div align="center">
  <a href="documentation.html"><img src="pix/gtkmm_logo.gif" border="0" alt="Logo" /><br /></a>
  </div>
  <center>
  <h1>New: gtkmm 2</h1>
  <para>
  <a href="gtkmm2/">gtkmm2</a> is not yet as complete as gtkmm 1.2, but it's already easier to use. And we also have a GNOME2 version of gnomemm.<br/>Here's an <a href="gtkmm2/">overview</a>, with documentation and status.
  </para>
  </center>
</td></tr>

<tr><td colspan="2">
<table border="0">
<tr><td colspan="2">
<h1>gtkmm</h1>
gtkmm (previously known as Gtk--) is the official C++ interface for the popular GUI library REF_GTK(GTK+).  Highlights include
typesafe callbacks, widgets extensible via inheritance and a comprehensive set of widget classes that can be freely combined to quickly create complex user interfaces. gtkmm is free software distributed under the GNU Library
General Public License(<a href="license.html">LGPL</a>).
<h1>gnomemm</h1>
gnomemm (Previously known as Gnome--) is a set of powerful C++ bindings for the REF_GNOME(GNOME) libraries, which provide additional functionality above GTK+/gtkmm. 
<h1>libsigc++</h1>
Also, be sure to check out our companion library, REF_SIGC(libsigc++).
Due to popular demand our signal/slot library is now provided
separately for use in non-GUI code. Thus
all the power of typesafe callbacks and flexible adaptor classes is
available for use, without changes to the C++ language.  
<h1>Frequently Asked Questions</h1>
Here's an <a href="docs/gtkmm-faq.html">FAQ</a> about gtkmm and gnomemm.
If you are curious about the status of gtkmm in relation to 
GNOME and Inti, please read this <a href="statement.html">
statement</a> regarding the differences.
<h1>Latest Versions</h1>
<h2>Stable</h2>
gtkmm release: 
<b><a href="download.html">GTK_VERSION_STABLE</a></b><br />
gnomemm release: 
<b><a href="download.html">GNOME_VERSION_STABLE</a></b><br />
<h2>Unstable (Development)</h2>
gtkmm release: 
<b><a href="download.html">GTK_VERSION_DEVEL</a></b><br />
gnomemm release: 
<b><a href="download.html">GNOME_VERSION_DEVEL</a></b><br />
LINK_SF(news/,ICON(news,News))
Current news about the latest version is on the LINK_SF(news/,news page)
<h1>New gtkmm related libraries</h1>
Be sure to check out <a href="http://bakery.sourceforge.net/">Bakery</a>
a framework for constructing gnomemm applications.  It allows for
quick use of the Document/View architecture.  Also for gtkmm, 
<a href="http://gtkextramm.sourceforge.net/"> GtkExtra--</a> for those 
extra widget wrappers.
<p />

</td>
</tr>
</table>
FOOTER(V)
