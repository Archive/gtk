include(base.m4)
HEADER(Examples,examples)

dnl SECTION(Examples)
We are in the process of converting the entire GTK+ tutorial 
over to gtkmm.  Although the text is not finished yet, most
of the examples for Gtk-- version 1.1 are.  You can browse through them here.
<p>
If you wish to contribute to these examples or the tutorial,
be sure to mail us.
<p>
Note: All examples require the latest stable distribution.
<p>
<ul>
define(EXAMPLES,[[patsubst(patsubst(esyscmd([[ls examples/*.html|sort]]),[[examples/]],[[]]),[[.html]],[[,]])]])
define(LOOP,[[ifelse($#,0,,$#,1,[[$1]],[[
<li><a href="examples/$1.html">$1</a>
LOOP(shift($@))]])]])
LOOP(EXAMPLES)
</ul>

FOOTER()
