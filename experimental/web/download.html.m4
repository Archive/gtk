include(base.m4)
HEADER(Downloading,download)

SECTION(Source Packages,source)
LINK_SF(project/showfiles.php,<img src="pix/save.gif" vspace="0" border="0" width="24" height="24" alt="*" />)
The source packages for gtkmm are available on the
LINK_SF(project/showfiles.php,sourceforge download area).
Libsigc++ (1.0.x for gtkmm 1.2, and 1.1.x for the unstable gtkmm 1.3) must also be installed, available 
<a href="http://sourceforge.net/project/showfiles.php?group_id=1970">here</a><br />
<p />
<p />

dnl SECTION(Binary packages)
dnl <i> Not available yet </i>
dnl (direct links may not be to latest package, see maintainer links below)
dnl <ul>
dnl   <li> Intel x86 (RedHat 6.x)
dnl   <ul>
dnl     <li>RPM: REF_HVR(gtk--1.0.2-1.i386.rpm,RPMS/i386/gtk---1.0.2-1.i386.rpm) (~1.6M)
dnl     <li>DEVEL RPM: REF_HVR(gtk---devel-1.0.2-1.i386.rpm,RPMS/i386/gtk---devel-1.0.2-1.i386.rpm) (~600k)
dnl     dnl <li>DEVEL STATIC RPM: REF_BASE(gtk---staticlibs-1.0.0-2.i386.rpm,gtk---staticlibs-1.0.0-2.i386.rpm) (~8M)
dnl   </ul>
dnl   <li> Alpha (RedHat 6.0) 
dnl   <ul>
dnl     <li>RPM: REF_HVR(gtk---1.0.2-1.alpha.rpm,RPMS/alpha/gtk---1.0.2-1.alpha.rpm) (~3.0M)
dnl     <li>DEVEL RPM: REF_HVR(gtk---devel-1.0.2-1.alpha.rpm,RPMS/alpha/gtk---devel-1.0.2-1.alpha.rpm) (~600k)
dnl   </ul>
dnl   <li> PowerPC (YellowDog 1.0) 
dnl   <ul>
dnl     <li>RPM: REF_HVR(gtk---1.0.2-1.ppc.rpm,RPMS/ppc/gtk---1.0.2-1.ppc.rpm) (~2.3M)
dnl     <li>DEVEL RPM: REF_HVR(gtk---devel-1.0.2-1.ppc.rpm,RPMS/ppc/gtk---devel-1.0.2-1.ppc.rpm) (~700k)
dnl   </ul>
dnl <li>DEB: REF_BASE(libgtkmm-dev_1.0.0-1.deb, libgtkmm-dev_1.0.0-1.deb) (~500k)
dnl </ul>
<p />
<p />

SECTION(Links to package maintainer web pages and ftp sites,links)
<ul>
<li><a href="http://www.hvrlab.org/~hvr/gtk--/">rpm</a> packages web site by Herbert Valerio Riedel</li>
dnl <li><a href="ftp://ftp.falsehope.com/pub/gtk--">rpm packages</a> ftp site by Ryan Weaver.
</ul>
</td></tr>
<tr><td colspan="2">

dnl Apparently these experimental deb packages aren't there anymore.
dnl SECTION(Way to get experimental deb packages,deb)
dnl <ul>
dnl <li>It works only with Debian potato(=unstable).</li>
dnl <li>If you use apt, put this in /etc/apt/sources.list:
dnl <pre>
dnl deb http://www.debian.org/~jules/gnome-stage-2 unstable main
dnl </pre></li>
dnl <li>install the gtkmm library
dnl <pre>
dnl apt-get install libgtkmm-dev
dnl </pre>
dnl </li>
dnl </ul>

FOOTER()
