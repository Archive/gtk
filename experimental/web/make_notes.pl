#!/usr/bin/perl


print "include(base.m4)
HEADER(Release notes,notes)
";
parse_notes("ChangeLog-1.1");
print "<hr noshade=\"noshade\" />\n";
parse_notes("ChangeLog-1.0");
print "FOOTER()\n";


sub parse_notes
{
local($file) = shift (@_);
open(FILE,$file);
while (<FILE>)
  {
    s/&/&amp;/g;
    s/</&lt;/g;
    s/>/&gt;/g;
    if (/^Release: (.*)/) {print "<h3>$1</h3>\n";}
    elsif (/^Get: (\S+)/) 
      {
        $href=$1;
        $name=$1;
        $name=~s/.*\///g;
        print "Get <a href=\"$href\">$name</a>\n";
      }
    elsif (/^=========/) {print "<hr noshade=\"noshade\" />\n";}
    elsif (/^$/) {print "<p />\n";}
    elsif (/^ +\* (.*)$/) 
      {
       print "<ul>\n";
       print "<li>$1\n";
       while (<FILE>)
         {
          s/&/&amp;/g;
          s/</&lt;/g;
          s/>/&gt;/g;
          (/^\s*$/) && last;
          if (/^ +\* (.*)$/) {print "</li><li>$1\n";}
          else {print $_;}
         }
       print "</li></ul>\n";
      }
    else {print $_;}
  }
close(FILE);

}
