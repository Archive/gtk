#!/bin/sh

files="
arrow/arrow.cc
arrow/direction.cc
aspectframe/aspectframe.cc
base/base.cc
buttonbox/buttonbox.cc
buttons/buttons.cc
clist/clist.cc
drawingarea/drawingarea.cc
entry/entry.cc
eventbox/eventbox.cc
filesel/filesel.cc
fixed/fixed.cc
frame/frame.cc
helloworld/helloworld.cc
helloworld2/helloworld2.cc
idle/idle.cc
input/input.cc
label/label.cc
list/list.cc
menu/menu2.cc
notebook/notebook.cc
packbox/packbox.cc
packer/packer.cc
paned/paned.cc
pixmap/pixmap.cc
progressbar/progressbar.cc
radiobuttons/radiobuttons.cc
rulers/rulers.cc
scribble-simple/scribble-simple.cc
scrolledwin/scrolledwin.cc
spinbutton/spinbutton.cc
statusbar/statusbar.cc
table/table.cc
text/text.cc
timeout/timeout.cc
toolbar/toolbar.cc
tree/tree.cc
wheelbarrow/wheelbarrow.cc"

mkdir examples
for file in $files;
do
  name=`basename $file .cc`
  echo ==== $file $name
  perl cc2html.pl < ../../examples/$file > examples/$name.html
done

