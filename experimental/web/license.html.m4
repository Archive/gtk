include(base.m4)
HEADER(License,license)
gtkmm is licensed under the GNU Library General Public License
for all platforms.  Our intent in licensing it in this way is to
provide it for use through shared libraries in all projects both 
open and proprietary.  Other GNU projects may of course integrate
and link in a static manner.  The full body of the license is
provided for your inspection.

<p />
This is the only license which grants you use of the software, so
if you do not agree to its terms, you may not use this software.

<p />
<hr noshade="noshade" width="30%" />
<p />
NEWSECTION()
preinclude(../../COPYING)
FOOTER()
