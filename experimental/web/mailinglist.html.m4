include(base.m4)

HEADER(Mailinglist,mailinglist)

SECTION(Where is the gtkmm mailinglist?,where)

<ul>
<li>Mailing list: <a href="mailto:gtkmm-main@lists.sourceforge.net">gtkmm-main@lists.sourceforge.net</a>.<br />
To subscribe, go to the <a href="http://lists.sourceforge.net/mailman/listinfo/gtkmm-main">subscribe page ICON(mail,Mailing List)</a>.
</li>
<li>For general GTK discussion, use the <a href="http://www.gtk.org/mailinglists.html">gtk-list</a>.
</li>
</ul>

SECTION(What subject material is appropriate for the list?,subject)
<br />
The gtkmm mailing list is primarily used for discussion of 
development, bug reports, suggestions, and improvements 
of the gtkmm wrapper itself.  There is also some discussion of use of the wrapper. Questions about the underlying GTK+ widget set 
are more likely to be answered quickly through the
gtk-list.
<p />

SECTION(Are there archives of the list?,archives)
<br />
The most complete archive is located at 
<a href="http://www.progressive-comp.com/Lists/?l=gtkmm&amp;r=1&amp;w=2#gtkmm">MARC</a>. Another archiver is available at
<a href="http://www.geocrawler.com/lists/3/SourceForge/1110/0/">Geocrawler</a>.
<p />

FOOTER()


