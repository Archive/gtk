include(base.m4)
HEADER(Tutorials,documentation,tutorials)

SECTION(Widgets)
<ul>
<li>REF_BASE(hello.cc,hello.cc) - A hello world in gtkmm
<li>REF_BASE(gtkmm-faq.html)
</ul>

SECTION(Signal system)
<ul>
<li>REF_BASE(tutorial,readme_signal.html) of signal framework
<li>REF_BASE(instructions,signal_system_without_gtk.html) of how to use gtkmm's signal system without gtkmm.
<li>REF_BASE(document,connects.html) about available signals and connect()
 function signatures.
</ul>
<br>
<br>
<br>
<br>
<br>
<br>
FOOTER()


