include(base.m4)
HEADER(Other,other,other)

SECTION(Screenshots)
<ul>
<li><a href="http://www.gtk.org/screenshots/">GTK+ screenshots</a>
<li><a href="http://www.gnome.org/screenshots/index.shtml">gnome desktop screenshots</a>
</ul>

SECTION(Bad or obsolete docs)
<ul>
<li>REF_BASE(tiny document,mapping_names.html) about how C's function and class names map to C++'s naming convention used in gtkmm.
<li>REF_BASE(porting.txt,porting.txt) file describes how to port a Qt/KDE application to use gtkmm.
<li>REF_BASE(range.gen_h, range.gen_h) example of how to create C++ wrapper for your C widget.
</ul>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
FOOTER()
