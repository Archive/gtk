#!/usr/bin/perl -w

$verbose = 0;

@ignorethese = qw(
		  gtk_accel_group_attach
		  gtk_accel_group_detach

		  gtk_button_box_child_requisition

                  gtk_entry_adjust_scroll

                  gtk_toolbar_append_element
                  gtk_toolbar_append_item
                  gtk_toolbar_append_widget
                  gtk_toolbar_insert_item
                  gtk_toolbar_prepend_element
                  gtk_toolbar_prepend_item
                  gtk_toolbar_prepend_widget

                  gtk_menu_factory_add_entries
                  gtk_menu_factory_add_subfactory
                  gtk_menu_factory_destroy
                  gtk_menu_factory_find
                  gtk_menu_factory_remove_entries
                  gtk_menu_factory_remove_paths
                  gtk_menu_factory_remove_subfactory

                  gtk_container_queue_resize
                  gtk_container_clear_resize_widgets
                  gtk_container_arg_set
                  gtk_container_arg_get
                  gtk_container_child_args_collect
                  gtk_container_child_arg_get_info
                  gtk_container_forall
                  gtk_container_child_composite_name
                  gtk_container_block_resize
                  gtk_container_unblock_resize
                  gtk_container_need_resize

                  gtk_ctree_show_stub
                  gtk_ctree_set_reorderable
                  gtk_ctree_set_use_drag_icons
                  gtk_ctree_real_select_recursive

                  gtk_item_factory_create_item
                  gtk_item_factory_create_items_ac

                  gtk_menu_get_attach_widget

                  gtk_object_arg_set
                  gtk_object_arg_get
                  gtk_object_args_collect
                  gtk_object_arg_get_info
                  gtk_object_class_add_user_signal
                  gtk_object_set
                  gtk_trace_referencing
                  gtk_option_menu_get_menu

                  gtk_packer_add_defaults

                  gtk_paned_compute_position

                  gtk_progress_bar_construct
                  gtk_rc_parse_color
                  gtk_rc_parse_priority
                  gtk_rc_parse_state

                  gtk_widget_destroy
                  gtk_widget_freeze_accelerators
                  gtk_widget_thaw_accelerators
                  gtk_widget_reset_shapes
                  gtk_widget_get
                  gtk_widget_getv

                  gtk_widget_set
                  gtk_widget_setv
                  gtk_widget_ref
                  gtk_widget_unref

		  gtk_window_set_focus
		  gtk_window_set_default
		  gtk_window_removed_embedded_xid
		  gtk_window_add_embedded_xid

		  );

%gtkincludes = ('base' => '',
                'buttonbox' => 'gtkbbox.h gtkhbbox.h gtkvbbox.h',
                'colorselection' => 'gtkcolorsel.h',
                'colorselectiondialog' => 'gtkcolorsel.h',
                'fileselection' => 'gtkfilesel.h',
                'fontselection' => 'gtkfontsel.h',
                'menupath' => 'gtkmenufactory.h',
                'widget' => 'gtkwidget.h gtkdnd.h');

#$GTKPATH = "/home/glaurent/gtk/gtk+/gtk";
$GTKPATH = "/home/cvs/gtk+/gtk";
# $GTKPATH = "/home/glaurent/gtk/gtk+-1.0.5/gtk";
sub can_ignore {
    my $gtkcall = shift;

  return (($gtkcall =~ /[sg]et_.?adjustments?$/) ||
          grep /$gtkcall/, @ignorethese)
  }

sub get_gtk_include {
    my $gtkmmfile = shift;
    my $res;
    my $root = $gtkmmfile;

    $root =~ s%(^.*/)|(\.gen_h)%%g;

    unless (exists $gtkincludes{$root})  {
	open FILE, "$gtkmmfile";
	while(<FILE>) {
	    if (m{^\#include\s+<gtk/(gtk$root\.h)}) {
		$res = $1;
		last;
	    }
	}
    }
    else {  $res = $gtkincludes{$root} };

    print "gtk include : $res\n" if $verbose;

    return $res;
}
sub check_widget_api {
    my($gtkmmfile, $gtkfiles) = @_;
    my(%gtkapi, %gtkmmapi, @gtkmmsignals, @missing_wrappers);
    my($gtkmmsig, $is_wrapped_as_signal);

  warn "No gtkfile found for $gtkmmfile\n" and return
      unless defined $gtkfiles;

    foreach $gtkfile (split /\s+/, $gtkfiles)  {
    open GTK, "$GTKPATH/$gtkfile" or
	warn "Couldn't open gtk '$gtkfile' : $!\n" and return;

    while (<GTK>) {

      # print class name
	print "Gathering API of $1\n" if /^struct _(Gtk.*)Class/ && $verbose;
      # skip the gtk_*_new() and gtk_*_get_type() which we never use
	next if /\#|_new|_get_type/;

	if(/^\s*(\w+\s*\*?)?\s+(gtk_[a-z]+_\w+)\s*\(/) {
        # regular function
	    $gtkapi{$2} = 1;
	    print "Adding '$2' (func) to gtkapi ($gtkfile : $.)\n" if $verbose;

	} elsif(/^\s+\w+\s+\(\*\s*(\w+)\)/) {
        # gtk signal
	    $gtkapi{$1} = 1;
	    print "Adding '$1' (signal) to gtkapi ($gtkfile : $.)\n" if $verbose;
	}
    }

    close GTK;
}

    open GTKMM, $gtkmmfile or die "Couldn't open gtkmm $gtkmmfile : $!\n";

    while (<GTKMM>) {
	s%//.*$%%g;                 # weed out comments
	if(/(gtk_[a-z]+_\w+)\s*\(/) {
	    $gtkmmapi{$1} = 1;
	    print "Adding '$1' (func) to gtkmmapi ($gtkmmfile : $.)\n" if $verbose;
	} elsif(/SIGNAL_.*SPEC\([\w\*]+\s+(.+)\(/) {
	    $signal_name = $1;
	    $signal_name =~ s/^.+@//; # remove possible leading 'Gtktype@'
	    $signal_name =~ s/_c$//; # remove possible trailing '_c'
      print "Adding '$signal_name' (signal) to gtkmmapi ($gtkmmfile : $.)\n"
	  if $verbose;
	    $gtkmmapi{$signal_name} = 1;
	    push @gtkmmsignals, $signal_name;
	}
    }

    close GTKMM;
  # Dump missing methods
    foreach $gtkcall (keys %gtkapi)  {
	unless (exists $gtkmmapi{$gtkcall}) {
	    $is_wrapped_as_signal = 0;
	    foreach $gtkmmsig (@gtkmmsignals) {
        if($gtkcall =~ /$gtkmmsig/)
	{
            # print "'$gtkcall' is implemented as signal '$gtkmmsig'\n";
            $is_wrapped_as_signal = 1; last;
	}
    }
      push @missing_wrappers, "\t$gtkcall\n" unless $is_wrapped_as_signal ||
	  can_ignore $gtkcall;
	}
    }

  print "%%%%%%%%%%%%%%%%%% Checking $gtkmmfile against $gtkfiles\n",
  sort @missing_wrappers, "%%%%%%%%%%%%%%%%% Finished\n\n"
      if($#missing_wrappers > -1);
}

foreach $gen_h (@ARGV) {
    check_widget_api $gen_h, get_gtk_include $gen_h;
}
