%option noyywrap
%option yylineno
%option stack

%{

//#define LEXER_TEST

#include <fstream>
#include <unistd.h>
#include <string>

#ifdef LEXER_TEST
#define WRAPCLASSDECL        1001
#define WRAPMETHODDECL       1002
#define WRAPSTATICMETHODDECL 1003
#define SIGNALDECL           1004
#define ENDCLASSDEF          1005 
#define CONST                1006
#define VOLATILE             1007
#define SYMNAME              1008 
#define SIGTYPE              1009
#define TYPESPECIFIER        1010 

#define PRIVATESECTION       2000 
#define DOCSECTION           2001 
#define IMPLSECTION          2002 
#define CLASSSECTION         2003 

typedef union {
  int ival;
  string* sval;
} YYSTYPE;

YYSTYPE yylval;

extern "C"
{
void yyerror(char *s);
}

#else
#include "gensig.h"
#include "y.tab.h"

#pragma }

extern "C"
{
void yyerror(char *s);
int yylex(...);
int yyparse(...);

}

#endif

/* initial code */
float GtkVersion=1.1;
int gtk_cond_if=false;
int gtk_cond_print=false;

int par_depth = 0;
int brace_depth = 0;

/* pattern declarations */
%}

string  \"[^\n"]+\"
symbols \*|&|<|>|,|\.

ws    [ \t]+
wsn [ \t]*

alpha   [A-Za-z]
dig     [0-9]

ccomment   \/\*
cppcomment \/\/.*$

 // Symbol name
symname [a-zA-Z_][a-zA-Z_0-9]+|[a-zA-Z_]
gtkcast [A-Z_]+

num1    [-+]?{dig}+\.?([eE][-+]?{dig}+)?
num2    [-+]?{dig}*\.{dig}+([eE][-+]?{dig}+)?
number  {num1}|{num2}
version {dig}+\.{dig}+

ifgtk ^{wsn}#ifgtk{ws}
elsegtk ^{wsn}#elsegtk
endifgtk ^{wsn}#endifgtk


 // Class definition/declaration
classdef class
classderivetype public|protected|private

typespecifier    int|bool|void|char|float|double|short|long|signed|unsigned

wrapclassdecl ^{wsn}WRAP_CLASS
wrapmethdecl ^{wsn}WRAP_METHOD
wrapsmethdecl ^{wsn}WRAP_STATIC_METHOD

signaldecl ^{wsn}SIGNAL_SPEC
signaltype vfunc|proxy|both|translate


 // Define states
%x CCOMMENT
%x CPPCOMMENT

%s GTK_COND_IF
%s GTK_COND_WEED

%x DECL_CHAR
%x DECL_STRING
%x WRAP_CHAR
%x WRAP_STRING

%s IN_WRAP
%s IN_DECL
%s END_DECL

%s CLASS_SECTION
%s DOCUMENTATION_SECTION
%s PRIVATE_SECTION
%s IMPL_SECTION



%%

%{ // Trouble character  (crashes m4)
%}
"`"  { fputc('\'',yyout); }



%{ // Comment handling  
%}

{ccomment}  {
       ECHO; 
       yy_push_state(CCOMMENT);
     }

<CCOMMENT>
  {
   "`"            fputc('\'',yyout);
   [^*`]*         ECHO;
   "*"+[^*`/]*    ECHO;
   "*"+"/"        ECHO; yy_pop_state();
  }

{cppcomment} {
       ECHO;
       yy_push_state(CPPCOMMENT);
     }

<CPPCOMMENT>
  {
   "`"            fputc('\'',yyout);
   [^`\n]*        ECHO;
   "\n"           ECHO; yy_pop_state(); yylineno--;
  }



%{ // Version control
%}

<GTK_COND_IF>
  {
   {version}  {
       gtk_cond_if=true;
       yy_pop_state();
       if ( fabs(atof(yytext)-GtkVersion) < 0.001 )
         {
           gtk_cond_print=true; 
         }
       else
         {
           yy_push_state(GTK_COND_WEED);    
         }
      }
   .             yyerror("bad gtk version");
  }

<GTK_COND_WEED>
  {
   {ifgtk}    {yy_pop_state(); yy_push_state(GTK_COND_IF);}
   {elsegtk}  {if (!gtk_cond_print) {yy_pop_state(); gtk_cond_print=true;} }
   {endifgtk} {
       yy_pop_state();  
       gtk_cond_if=false;  
       gtk_cond_print=false;
     }
   "\n"       ECHO;
   [^\n]*     
  }

{ifgtk}   {yy_push_state(GTK_COND_IF);}

{elsegtk} {
    if (!gtk_cond_if)  yyerror("#elsegtk encountered without #ifgtk");
    yy_push_state(GTK_COND_WEED);
  }

{endifgtk} {
    if (!gtk_cond_if)  yyerror("#endgtk encountered without #ifgtk");
    gtk_cond_if=false;
    gtk_cond_print=false;
  }



%{ // Declarations
%}
<DECL_STRING>{
    {symname} {
       yylval.sval = new string(yytext);
       return SYMNAME;
     }
    \\.  {printf(yytext);}
    "\"" {yy_pop_state();return '\"';}
    .    {printf(yytext);}
  }

<DECL_CHAR>{
    \\.  {printf(yytext);}
    "\'" {yy_pop_state();return '\'';}
    .    {printf(yytext);}
  }

<IN_DECL>{
   \{|\} {yyerror("braces in declaration.");}

   "(" {
       par_depth++;
       return '(';
     }

   ")" {
       par_depth--;
       if (!par_depth) 
         {yy_pop_state();
          yy_push_state(END_DECL);
          return ')';
         }
       return ')';
     }
   
   "\"" {
       yy_push_state(DECL_STRING);
       return '\"';
     }

   "const"    { return CONST; }

   "volatile" { return VOLATILE; }
 
   {typespecifier} {
      yylval.sval = new string(yytext);
      return TYPESPECIFIER;
     }

   {signaltype} {
       yylval.sval = new string(yytext);
       return SIGTYPE;
     }

   {symbols} {
       return yytext[0];
     }

   {symname} {
       yylval.sval = new string(yytext);
       return SYMNAME;
     }

   {ws}
   "\n"  ECHO;

   . {printf(yytext);}
  }

<END_DECL>{
    ";" {yy_pop_state();}
    {ws}
    .   {yyerror("declaration missing ;");}
  }



%{ // Declarations
%}
<WRAP_STRING>{
    \\.  {ECHO;}
    "\"" {ECHO; yy_pop_state();}
    .    {ECHO;}
  }

<WRAP_CHAR>{
    \\.  {ECHO;}
    "\'" {ECHO; yy_pop_state();}
    .    {ECHO;}
  }

<IN_WRAP>{
    "\'" {
        ECHO; 
        yy_push_state(WRAP_CHAR);
      }

    "\"" {
        ECHO; 
        yy_push_state(WRAP_STRING);
      }

    "{" {
        brace_depth++;
        ECHO; 
      }

    "}" {
        if (brace_depth) brace_depth--;
        if (!brace_depth)
          {
           yy_pop_state();
           return ENDCLASSDEF;
          }
        ECHO; 
      }

    {wrapmethdecl}   {yy_push_state(IN_DECL);return WRAPMETHODDECL;}
    {wrapsmethdecl}  {yy_push_state(IN_DECL);return WRAPSTATICMETHODDECL;}
    {signaldecl}     {yy_push_state(IN_DECL);return SIGNALDECL;} 

    {wrapclassdecl}  {yyerror("duplicate WRAP_CLASS");}
  }


%{ // Section specific marks
%}
<CLASS_SECTION>{wrapclassdecl} {
   yy_push_state(IN_WRAP);
   yy_push_state(IN_DECL);
   par_depth=0;
   brace_depth=1;
   return WRAPCLASSDECL;
  }

%{ // Whitespace
%}
{wsn}\n    {fputc('\n',yyout);}  // kill trailing ws
{ws}       {ECHO;}            // rest falls through


%{ // Handle the various xx_START marks
%}
PRIVATE_START{wsn};\n       {
    yy_push_state(PRIVATE_SECTION);
    return PRIVATESECTION;
  }
IMPL_START{wsn};\n          {
    yy_push_state(IMPL_SECTION);
    return IMPLSECTION;
  }
DOCUMENTATION_START{wsn};\n {
    yy_push_state(DOCUMENTATION_SECTION);
    return DOCSECTION;
  }
CLASS_START{wsn};\n         {
    yy_push_state(CLASS_SECTION);
    return CLASSSECTION;
  } 
%%



void yyerror(char *s)
{
  fprintf(stderr, "error at line %u : %s\n", yylineno, s);
  exit(EXIT_FAILURE);
}


#ifdef LEXER_TEST
int main( int  argc, char** argv )
  {
    int token;
    if (argc!=3) exit(-1);
    yyin=fopen(argv[1],"r");
    yyout=fopen(argv[2],"w");

    while((token=yylex()) != 0)
      {
       switch (token)
         {
          case WRAPCLASSDECL: 
            printf("WRAPCLASSDECL ");
            break;
          case WRAPMETHODDECL: 
            printf("WRAPMETHODDECL ");
            break;
          case WRAPSTATICMETHODDECL: 
            printf("WRAPSTATICMETHODDECL ");
            break;
          case SIGNALDECL: 
            printf("SIGNALDECL ");
            break;
          case CONST: 
            printf("CONST ");
            break;
          case SIGTYPE: 
            printf("SIGTYPE ");
            break;
          case TYPESPECIFIER: 
            printf("TYPE ");
            break;
          case SYMNAME: 
            printf("SYM ");
            break;
          case ')': 
            printf(") ");
            if (!par_depth) printf("\n");
            break; 
          case ENDCLASSDEF:
            printf("ENDCLASS\n");
            break; 
          default:
            printf("%c ",token);
         }
      };
    return 0;
  }
#endif
