%{
#include <stdio.h>
#include <fstream>
#include <string>
#include <list>
#include "gensig.h"

WidgetDef noWidget;
WidgetDef *currentWidget=&noWidget;
list<WidgetDef*> widgets;

OutputChannel *headerfile,*implfile,*privatefile;

extern "C"
{
void yyerror(char *s);
int yylex();
int yyparse();
char *getenv();
}

extern FILE *yyout, *yyin;

%}

%union {
  int ival;
  string* sval;
  ArgList* aval;
  FunctionDef* fval;
}

%token CLASS WRAPCLASSDECL WRAPMETHODDECL WRAPSTATICMETHODDECL SIGNALDECL ENDCLASSDEF
%token CLASSSECTION DOCSECTION PRIVATESECTION IMPLSECTION
%token CONST VOLATILE
%token <sval> SYMNAME  
%token <sval> SIGTYPE 
%token <sval> TYPESPECIFIER 

%token DUMMY

%%

input: classwrap
| classwrap input 
;

classwrap: wrapdef 
| endclassdef 
| wrap_method_decl
| signal_decl
| section_decl
| wrap_static_method_decl
;

/*
 * widget wrapping declaration :
 *
 * WRAP(CppName,CppParent,CName,CParent,CCast,CCheck,basename)
 * WRAP(Gtk_Button,Gtk_Bin,GtkButton,GtkBin,GTK_BUTTON,GTK_IS_BUTTON,button)
 */
wrapdef: WRAPCLASSDECL '(' SYMNAME ',' SYMNAME ',' SYMNAME ',' SYMNAME ',' SYMNAME ',' SYMNAME ',' SYMNAME ')' 
{
#ifdef DEBUG
  cout << "WRAPCLASSDECL" << endl;
  cout << "1: " << *($3) <<endl;
  cout << "2: " << *($5) <<endl;
  cout << "3: " << *($7) <<endl;
  cout << "4: " << *($9) <<endl;
  cout << "5: " << *($11) <<endl;
  cout << "6: " << *($13) <<endl;
  cout << "7: " << *($15) <<endl;
#endif
  WidgetDef *w=new WidgetDef($<sval>3,$<sval>5,$<sval>7,$<sval>9,$<sval>11,$<sval>13,$<sval>15);
  currentWidget=w;
  widgets.insert(widgets.end(),w);
  w->print_decl_begin(yyout);
}
;

typespecifiers: TYPESPECIFIER
{
  $<sval>$ = $<sval>1;
}
| typespecifiers TYPESPECIFIER
{
  *($<sval>1)+=" ";
  *($<sval>1)+=*($<sval>2);
  delete ($<sval>2);
  $<sval>$ = $<sval>1;
}
;

cv_qualifier: CONST
{
  $<sval>$ = new string("const");
}
| VOLATILE
{
  $<sval>$ = new string("volatile");
}
;

identifier: SYMNAME
{
  $<sval>$ = $<sval>1;
}
| template_id
{
  $<sval>$ = $<sval>1;
}
| typespecifiers
{
  $<sval>$ = $<sval>1;
}
;

typename: typename '*'
{
  *($<sval>1)+="*";
  $<sval>$ = $<sval>1;
}
| typename '&'
{
  *($<sval>1)+="&";
  $<sval>$ = $<sval>1;
}
| typename cv_qualifier
{
  *($<sval>1)+=" ";
  *($<sval>1)+=*($<sval>2);
  delete ($<sval>2);
  $<sval>$ = $<sval>1;
}
| cv_qualifier identifier
{
  *($<sval>1)+=" ";
  *($<sval>1)+=*($<sval>2);
  delete ($<sval>2);
  $<sval>$ = $<sval>1;
}
| identifier
{
  $<sval>$ = $<sval>1;
}
;


/*
 * Templates
 */
template_id: SYMNAME '<' template_argument_list '>'
{
  *($<sval>1)+="<";
  *($<sval>1)+=*($<sval>3);
  *($<sval>1)+=">";
  delete ($<sval>3);
  $<sval>$ = $<sval>1;
}
| SYMNAME '<' '>'
{
  *($<sval>1)+="<>";
  $<sval>$ = $<sval>1;
}
;
 
template_argument_list: template_argument_list ',' typename
{
  *($<sval>1)+=*($<sval>3);
  delete ($<sval>3);
  $<sval>$ = $<sval>1;
}
| typename
{
  $<sval>$ = $<sval>1;
}
;



/*
 * A list of coma-seperated C++ args 
 *
 * int, const char*, char&, unsigned long
 */
cpp_arglist: cpp_arglist ',' typename
{
  ($<aval>1)->push_arg($<sval>3);
  $<aval>$ = $<aval>1;
}
| typename
{
  $<aval>$ = new ArgList($<sval>1);
}
;

cpp_func_id: typename SYMNAME '(' ')'
{
  $<fval>$ = new FunctionDef($<sval>1,$<sval>2);
}
| typename SYMNAME '(' cpp_arglist ')'
{
  $<fval>$ = new FunctionDef($<sval>1,$<sval>2,$<aval>4);
}
| typename template_id  '(' cpp_arglist ')'
{
  $<fval>$ = new FunctionDef($<sval>1,$<sval>2,$<aval>4);
}
;

cpp_func: cpp_func_id
| cpp_func_id CONST
{
  ($<fval>1)->mark_const();
  $<fval>$ = $<fval>1;
}
;

wrap_method_decl: WRAPMETHODDECL '(' cpp_func ',' cpp_func ')'
{
#ifdef DEBUG
  cout<< "WRAP_METHOD 1:" 
      << ($<fval>3)->get_function() << "  2:"
      << ($<fval>5)->get_function() <<endl;
#endif
  MethodDef *method=new MethodDef($<fval>3,$<fval>5);
  currentWidget->push_method(method);
  method->print_decl(yyout);
}
;

wrap_static_method_decl: WRAPSTATICMETHODDECL '(' cpp_func ',' cpp_func ')'
{
#ifdef DEBUG
  cout<< "WRAP_STATIC_METHOD 1:" 
      << ($<fval>3)->get_function() << "  2:"
      << ($<fval>5)->get_function() <<endl;
#endif
  MethodDef *method=new StaticMethodDef($<fval>3,$<fval>5);
  currentWidget->push_method(method);
  method->print_decl(yyout);
}
;

endclassdef: ENDCLASSDEF
{
#ifdef DEBUG
  cout << "ENDCLASS"<<endl;
#endif
  currentWidget->print_decl_end(yyout);
  currentWidget=&noWidget;
  fputc('}',yyout);
}

/*
 * Signal Declaration :
 *
 * SIGNAL_SPEC("signame", both, void signame(int, char) );
 * SIGNAL_SPEC("signame", translate,
 *             void signame(GtkWidget*),
 *             void SigName(Gtk_Widget*));
 *                                           
 */
signal_decl: SIGNALDECL '(' '"' SYMNAME '"' ',' SIGTYPE ',' cpp_func ')' 
{
#ifdef DEBUG
  cout << " SIGNALDECL (\""<< *($<sval>4) <<"\", "<< *($<sval>7) <<","<< ($<fval>9)->get_function() << ")" <<endl;
#endif
  SignalDef *sigDef = SignalDef::Factory($7, $4, $<fval>9);
  currentWidget->push_signal(sigDef);
}
| SIGNALDECL '(' '"' SYMNAME '"' ',' SIGTYPE ',' cpp_func ',' cpp_func ')' 
{
  TranslateSignalDef *sigDef = new TranslateSignalDef($4,$<fval>9,$<fval>11);
  currentWidget->push_signal(sigDef);
};

section_decl: CLASSSECTION
{
  yyout=headerfile->out();
  env.print_linenum(yyout);
}
| IMPLSECTION
{
  yyout=implfile->out();
  env.print_linenum(yyout);
}
| DOCSECTION
{
  yyout=headerfile->out();
  env.print_linenum(yyout);
}
| PRIVATESECTION
{
  yyout=privatefile->out();
  env.print_linenum(yyout);
}
;

%%

int main(int argc, char *argv[])
{
  int rc;
  env.init(argc,argv);

  yyin=fopen(env.input_name().c_str(),"r");
  if (!yyin) 
    {
     cerr << "Failed to open file: "<< env.input_name().c_str() <<endl;
     exit (-1);
    }

  headerfile =new HeaderChannel(env.header_name());
  privatefile=new PHeaderChannel(env.private_name());
  implfile   =new SourceChannel(env.impl_name());
  yyout=headerfile->out();

  rc=yyparse();

  for (list<WidgetDef*>::iterator i=widgets.begin();i!=widgets.end();i++) 
    {
     (*i)->print_private(privatefile->out());
     (*i)->print_impl(implfile->out());
    }

  headerfile->process();
  privatefile->process();
  implfile->process();

  return rc;
}

