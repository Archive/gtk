/////////////////////////////////////////////////////////////////////////////
// List of arguments
/////////////////////////////////////////////////////////////////////////////

#include <string>
#include <list>
#include <stdio.h>

class ArgList
  {
    protected:
      typedef list<string*> List;
      int num_;
      List args_;

    public:
 
      ArgList();
      ArgList(string *s);
      ArgList(const ArgList& a);
 
      // return a coma seperated list of the args, for instance :
      // const int, char, long*
      string get_types() const ;
 
      // same, but with synthesized names :
      // const int p1, char p2, long* p3
      string get_both() const;

      // return the synthesized names only :
      // p1, p2, p3
      string get_names() const;
        
      void push_arg(string *s);
      void reset() ;

      bool empty() const { return args_.empty(); }
      int size() const   { return num_; }

      void erase_first_arg();

      ~ArgList();
  };

/////////////////////////////////////////////////////////////////////////////
// C Function definition (also used for signals)
/////////////////////////////////////////////////////////////////////////////

class FunctionDef
  {
    protected:
      string *name_;
      string *rettype_;
      ArgList *args_;
      bool isconst_;

    public:

      FunctionDef(string *rettype,
                  string *name,
                  ArgList* args=0,
                  bool isconst = false);

      FunctionDef(const FunctionDef& f);

      const string& get_name() const                { return *name_; }
      const string& get_return_type() const         { return *rettype_; }

      // returns the string equivenlent of this function
      string get_function();

      bool is_const() const            { return isconst_; }
      bool returns_void() const        { return *rettype_ == "void"; }
      bool has_args() const            { return args_->empty();}
      unsigned int get_nb_args() const { return args_->size(); }

      ArgList& args()                  { return *args_; } 
      const ArgList& get_args() const  { return *args_; }
   
      void mark_const()                { isconst_=true; }

  };

/////////////////////////////////////////////////////////////////////////////
// Signal definition
/////////////////////////////////////////////////////////////////////////////
class SignalDef
  {
    public:
      static SignalDef* Factory(string *sigtype,
                             string *name,
                             FunctionDef *funcdef);

      SignalDef(string *name,
             FunctionDef *funcdef);

      virtual ~SignalDef() {};

      virtual void print_proxy_decl(FILE *out,int sigId);
      virtual void print_impl_decl(FILE *out);
      virtual void print_impl(FILE *out,const string&,const string&);
      virtual void print_callback_decl(FILE *out);

      const string& get_gtk_name        () {return *name_;}
      const string& get_c_return_type   () {return funcdef_->get_return_type();}
      const string& get_c_name          () {return funcdef_->get_name();}
      const FunctionDef& get_func_def()    {return *funcdef_; }

    protected:
      static const string comaSpace; // ", "
      static const string closeM4Parenthesis; // ")dnl\n"
      static const string openQuote, closeQuote; // "`", "'"

      string *name_;
      FunctionDef *funcdef_;
  };

/////////////////////////////////////////////////////////////////////////////

class VFuncSignalDef : public SignalDef
  {
    public:
      VFuncSignalDef(string *name,
                  FunctionDef *funcdef)
        : SignalDef(name, funcdef) {}

      virtual ~VFuncSignalDef() {};

      virtual void print_proxy_decl(FILE *out,int sigId);
  };

/////////////////////////////////////////////////////////////////////////////

class ProxySignalDef : public SignalDef
  {
    public:
      ProxySignalDef(string *name,
                  FunctionDef *funcdef)
        : SignalDef(name, funcdef) {}

      virtual ~ProxySignalDef() {};

  //    virtual void print_impl_decl(FILE *out); // no-op
  };

/////////////////////////////////////////////////////////////////////////////

class BothSignalDef : public SignalDef
  {
    public:
      BothSignalDef(string *name,
                 FunctionDef *funcdef)
        : SignalDef(name, funcdef) {}

      virtual ~BothSignalDef() {};
  };

/////////////////////////////////////////////////////////////////////////////
class TranslateSignalDef : public SignalDef
  {
    public:
      TranslateSignalDef(string *name,
                      FunctionDef *funcdef)
        : SignalDef(name, funcdef) {}

      TranslateSignalDef(string *name,
                      FunctionDef* funcdef,FunctionDef* cppfuncdef)
        : SignalDef(name, funcdef),cppFuncDef_(cppfuncdef) {}

      virtual ~TranslateSignalDef() {};

//     virtual void print_proxy_decl(FILE *out);
//     virtual void print_impl_decl(FILE *out);

      void set_cpp_def (FunctionDef *cppFuncDef) 
        { cppFuncDef_ = cppFuncDef; }

      const string get_cpp_return_type () 
        { return cppFuncDef_->get_return_type(); }
      const string get_cpp_name        () 
        { return cppFuncDef_->get_name(); }

    protected:
      FunctionDef* cppFuncDef_;
  };

///////////////////////////////////////////////////////////////////////

class MethodDef
  {
    protected:
      FunctionDef *cFunc_;
      FunctionDef *cppFunc_;
    public:
      MethodDef(FunctionDef *cFunc,FunctionDef *cppFunc)
        : cFunc_(cFunc), cppFunc_(cppFunc) 
        {}
      
      virtual ~MethodDef();
      virtual void print_decl(FILE *out);
      virtual void print_impl(FILE *out,const string& cppName);
  };

class StaticMethodDef : public MethodDef
  {
   public:
      StaticMethodDef(FunctionDef *cFunc,FunctionDef *cppFunc)
        :MethodDef(cFunc,cppFunc)
        {}
      virtual ~StaticMethodDef();
      virtual void print_decl(FILE *out);
      virtual void print_impl(FILE *out,const string& cppName);
  };

///////////////////////////////////////////////////////////////////////

class WidgetDef
  {
   private:
     WidgetDef(const WidgetDef&);
   protected:
     string *cppName_;
     string *cppParent_;
     string *cName_;
     string *cParent_;
     string *cCast_;
     string *cCheck_;
     string *base_;
 
     list<SignalDef*> signals_;
     list<MethodDef*> methods_;

   public:
      WidgetDef();

      WidgetDef(string *cppName,
                string *cppParent,
                string *cName,
                string *cParent,
                string *cCast,
                string *cCheck,
                string *base);
      ~WidgetDef();

      void push_signal(SignalDef*);
      void push_method(MethodDef*);

      void print_decl_begin(FILE *);
      void print_decl_end(FILE *);
      void print_private(FILE *);
      void print_impl(FILE *);
  };

class OutputChannel
  {
    public:
      OutputChannel(const string &fname);
      virtual ~OutputChannel();

      // last line in file
      virtual string closing();

      FILE* out() { return out_; }

      void process();

    protected:
      FILE *out_;
      string fname_;

      static const char *const gensigM4;
  };

class HeaderChannel: public OutputChannel
  {
    public:
      HeaderChannel(const string &fname);
      virtual string closing();
  };

class PHeaderChannel: public OutputChannel
  {
    public:
      PHeaderChannel(const string &fname);
      virtual string closing();
  };

class SourceChannel: public OutputChannel
  {
    public:
      SourceChannel(const string &fname);
  };

/////////////////////////////////////////////////////////////////////////////
// Environment (holds file names and such )
/////////////////////////////////////////////////////////////////////////////
class Environment
  {
    protected:
      string name_;
      string prefix_;
      string srcdir_;
      string destdir_;
      string m4_include_;
      bool local_;
      bool doc_;
      bool debug_;
      bool gnome_;

    public:
      Environment();
      ~Environment();

      void usage();
      void init(int argc,char **argv) ;

      string header_name()  {return (destdir()+name_+".h");}
      string private_name() {return (destdir()+"private/"+name_+"_p.h");}
      string impl_name()    {return (destdir()+name_+".cc");}
      string input_name()   {return (srcdir()+name_+".gen_h");}
      string firewall()     {return "gtkmm_"+name_;} 

      string srcdir()  {return srcdir_+"/";}
      string destdir() {return destdir_+"/";}
 
      string m4_invoke();

      bool local() {return local_;}
      bool doc()   {return doc_;}
      bool debug() {return debug_;}

      void print_includes(FILE *);
      void print_linenum(FILE *);


  };

extern Environment env;
