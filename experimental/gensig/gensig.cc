#include <string>
#include <stdio.h>
#include <unistd.h>
#include "gensig.h"


Environment env;

// Class for holding argument lists
ArgList::ArgList()
  :num_(0)
  {}
 
ArgList::ArgList(string *s)
  :num_(1)
  {
    args_.insert(args_.begin(),s);
  }

ArgList::ArgList(const ArgList& a)
  {
    num_=a.num_;
    List::const_iterator i;
    for (i=a.args_.begin();i!=a.args_.end();i++)
      {
       args_.insert(args_.end(),new string(*(*i)));
      }
  }
 
// return a coma seperated list of the args, for instance :
// const int, char, long*
string ArgList::get_types() const 
  {
    string s;
    List::const_iterator i=args_.begin();
    while (i!=args_.end())
      {
        s+=*(*i);
        i++;
        if (i==args_.end()) break;
        s+=",";
      }
    return s;
  }
 
// same, but with synthesized names :
// const int p1, char p2, long* p3
string ArgList::get_both() const
  {
    char tmp[20];
    string s;
    int n=0;
    List::const_iterator i=args_.begin();
    while (i!=args_.end())
      {
       s+=*(*i);
       sprintf(tmp," p%d",n);
       s+=tmp;
       i++;
       n++;
       if (i==args_.end()) break;
       s+=",";
      }
    return s;
  }

// return the synthesized names only :
// p1, p2, p3
string ArgList::get_names() const
  { 
    char tmp[20];
    string s;
    int i=0;
    while (i<num_)
      {
       sprintf(tmp,"p%d",i);
       s+=tmp;    
       i++;
       if (!(i<num_)) break;
       s+=",";
      }
    return s;
  }
  
void ArgList::push_arg(string *s)
  {
    args_.insert(args_.end(),s);
    num_++;
  }

void ArgList::reset() 
  { 
    List::iterator i;
    for (i=args_.begin();i!=args_.end();i++)
      delete *i;
    args_.clear(); 
  }

void ArgList::erase_first_arg() 
  { 
    if(!args_.empty()) 
      args_.erase(args_.begin()); 
  }

ArgList::~ArgList()
  {
    reset();
  }

// Class for holding functions
FunctionDef::FunctionDef(string *rettype,
            string *name,
            ArgList* args=0,
            bool isconst = false) 
  {
   if (!name) name_=new string(); name_=name;
   if (!rettype) rettype_=new string(); rettype_=rettype;
   if (!args) args_=new ArgList(); else args_=new ArgList(*args);
   isconst_=isconst;         
  }

FunctionDef::FunctionDef(const FunctionDef& f)
  {
   name_=new string(*(f.name_));
   rettype_=new string(*(f.rettype_));
   args_=new ArgList(*(f.args_));
   isconst_=f.isconst_;
  }

string FunctionDef::get_function()
  {
   string s=*rettype_;
   s+=" ";
   s+=*name_;
   s+="(";
   s+=args_->get_types();
   s+=")";
   if (isconst_) s+=" const";
   return s;
  }

// Signals
const string SignalDef::comaSpace(", ");
const string SignalDef::closeM4Parenthesis(")dnl\n");
const string SignalDef::openQuote("`");
const string SignalDef::closeQuote("'");

SignalDef* SignalDef::Factory(string *sigtype,
                        string *name,
                        FunctionDef *funcdef)
  {
    string sig=*sigtype;
    delete sigtype;
    if(sig== "vfunc")
      return new VFuncSignalDef(name, funcdef);
    else if(sig== "proxy")
      return new ProxySignalDef(name, funcdef);
    else if(sig== "both")
      return new BothSignalDef(name, funcdef);
    else if(sig== "translate")
      return new TranslateSignalDef(name, funcdef);
    else
      {
      // This should never happen : the lexer will ensure only known
      // types are passed
        cerr << "SignalDef::Factory() : unknown sig type : "
             << sigtype << endl;
        throw EXIT_FAILURE;
      }
  }


SignalDef::SignalDef(string *name,
               FunctionDef *funcdef)
  : name_(name),
    funcdef_(funcdef)
  {}

// Proxy Declarations
void SignalDef::print_proxy_decl(FILE *out, int sigId)
  {
    fprintf(out,"GTKMM_PROXY_SIGDEF(%d,%s,%s,%s,`%s')\n",
        sigId,
        funcdef_->get_return_type().c_str(), 
        name_->c_str(),
        funcdef_->get_name().c_str(), 
        funcdef_->get_args().get_types().c_str()
      );
  }

void VFuncSignalDef::print_proxy_decl(FILE *out, int sigId)
  {}

void SignalDef::print_callback_decl(FILE *out)
  {
    fprintf(out,"GTKMM_PRIVATE_CALLBACK_DECL(%s,%s,`%s')\n",
        funcdef_->get_return_type().c_str(),
        funcdef_->get_name().c_str(),
        funcdef_->get_args().get_types().c_str()
      );
  }


// Impl Declarations

void SignalDef::print_impl_decl(FILE *out)
  {
    fprintf(out,"GTKMM_VFUNC_SIGDEF(%s,%s,`%s')\n",
        funcdef_->get_return_type().c_str(), 
        funcdef_->get_name().c_str(), 
        funcdef_->get_args().get_types().c_str()
      );
  }

void SignalDef::print_impl(FILE *fptr,const string& cppName, const string& cName)
  {
   fprintf(fptr,"GTKMM_SIG_IMPL_CALLBACK(%s,%s,%s,%s,`%s',`%s')\n",
        funcdef_->get_name().c_str(), 
        cppName.c_str(),
        cName.c_str(),
        funcdef_->get_return_type().c_str(), 
        funcdef_->get_args().get_both().c_str(),
        funcdef_->get_args().get_names().c_str()
      );
  }

///////////////////////////////////////////////////////////////////////

MethodDef::~MethodDef()
 {}

void MethodDef::print_impl(FILE *out,const string& cppName)
  {
   fprintf(out,"GTK_METHOD_IMPL(%s,%s,%s,`%s',%d,%s,`%s')\n",
       cppFunc_->get_return_type().c_str(),
       cppName.c_str(),
       cppFunc_->get_name().c_str(),
       cppFunc_->get_args().get_both().c_str(),
       cppFunc_->is_const(),
       cFunc_->get_name().c_str(),
       cppFunc_->get_args().get_names().c_str()
     );
  }

void MethodDef::print_decl(FILE *out)
  {
   fprintf(out,"GTK_METHOD_DECL(%s,%s,`%s',%d)",
       cppFunc_->get_return_type().c_str(),
       cppFunc_->get_name().c_str(),
       cppFunc_->get_args().get_types().c_str(),
       cppFunc_->is_const()
     );
  }

StaticMethodDef::~StaticMethodDef()
 {}

void StaticMethodDef::print_impl(FILE *out,const string& cppName)
  {
   fprintf(out,"GTK_STATIC_METHOD_IMPL(%s,%s,%s,`%s',%s,`%s')\n",
       cppFunc_->get_return_type().c_str(),
       cppName.c_str(),
       cppFunc_->get_name().c_str(),
       cppFunc_->get_args().get_both().c_str(),
       cFunc_->get_name().c_str(),
       cppFunc_->get_args().get_names().c_str()
     );
  }

void StaticMethodDef::print_decl(FILE *out)
  {
   fprintf(out,"GTK_STATIC_METHOD_DECL(%s,%s,`%s')",
       cppFunc_->get_return_type().c_str(),
       cppFunc_->get_name().c_str(),
       cppFunc_->get_args().get_types().c_str()
     );
  }



///////////////////////////////////////////////////////////////////////

WidgetDef::WidgetDef()
  :cppName_(new string()),cppParent_(new string()),cName_(new string()),
   cParent_(new string()),cCast_(new string()),
   cCheck_(new string()),base_(new string())
  {}

WidgetDef::WidgetDef(string *cppName,
                string *cppParent,
                string *cName,
                string *cParent,
                string *cCast,
                string *cCheck,
                string *base)
  :cppName_(cppName),cppParent_(cppParent),cName_(cName),
   cParent_(cParent),cCast_(cCast),cCheck_(cCheck),base_(base)
  {}

WidgetDef::~WidgetDef()
  {
   delete cppName_;
   delete cppParent_;
   delete cName_;
   delete cParent_;
   delete cCast_;
   delete cCheck_;
   delete base_;
    
   // need to empty list
  }

void WidgetDef::push_signal(SignalDef *s)
  {
   signals_.insert(signals_.end(),s);
  }

void WidgetDef::push_method(MethodDef *s)
  {
   methods_.insert(methods_.end(),s);
  }

void WidgetDef::print_decl_begin(FILE *fptr)
  { 
   fprintf(fptr,"LINE_GENERATED(%s)\n",env.header_name().c_str());
   fprintf(fptr,"GTKMM_WRAP_DECL(%s,%s,%s,%s,%s,%s)\n",
       cppName_->c_str(),
       cppParent_->c_str(),
       cName_->c_str(),
       cParent_->c_str(),
       cCast_->c_str(),
       base_->c_str()
     );
   env.print_linenum(fptr);
  }

void WidgetDef::print_decl_end(FILE *fptr)
  { 
   list<SignalDef*>::iterator i; 
   int n=0;
   fprintf(fptr,"LINE_GENERATED(%s)\n",env.header_name().c_str());
   fprintf(fptr,"GTKMM_SIG_PROXY_DECL\n");
   for (i=signals_.begin();i!=signals_.end();i++)
      (*i)->print_proxy_decl(fptr,n++);
   fprintf(fptr,"GTKMM_SIG_IMPL_DECL\n");
   for (i=signals_.begin();i!=signals_.end();i++)
      (*i)->print_impl_decl(fptr);
   fprintf(fptr,"GTKMM_SIG_DECL_END\n");
   env.print_linenum(fptr);
  }

void WidgetDef::print_impl(FILE *fptr)
  {
   list<SignalDef*>::iterator si; 
   list<MethodDef*>::iterator mi; 
   fprintf(fptr,"LINE_GENERATED(%s)\n",env.impl_name().c_str());
   fprintf(fptr,"GTKMM_CLASS_GETTYPE(%s)\n",
       cppName_->c_str()
     );

   // Class Init function
   fprintf(fptr,"GTKMM_CLASS_INIT_FUNCTION_START(%s,%s)\n",
       cppName_->c_str(),
       cName_->c_str()
     );
   for (si=signals_.begin();si!=signals_.end();si++)
      fprintf(fptr,"GTKMM_CLASS_INIT_FUNCTION(%s)\n",
         (*si)->get_gtk_name().c_str());
   fprintf(fptr,"GTKMM_CLASS_INIT_FUNCTION_END(%s,%s)\n",
       cppName_->c_str(),
       cName_->c_str()
     );

   // Proxy Names
   fprintf(fptr,"GTKMM_PROXY_NAMES_START(%s)\n",
       cppName_->c_str()
     );
   for (si=signals_.begin();si!=signals_.end();si++)
      fprintf(fptr,"GTKMM_PROXY_NAMES(%s)\n",
         (*si)->get_gtk_name().c_str());
   fprintf(fptr,"GTKMM_PROXY_NAMES_END()\n");

   //  Cast check
   fprintf(fptr,"GTKMM_GTK_IS_TYPE(%s,%s,%s)\n",
       cppName_->c_str(),
       cName_->c_str(),
       cCheck_->c_str()
     );

   // Implementations
   for (mi=methods_.begin();mi!=methods_.end();mi++)
      (*mi)->print_impl(fptr,*cppName_);
   for (si=signals_.begin();si!=signals_.end();si++)
      (*si)->print_impl(fptr,*cppName_,*cName_);
  }

void WidgetDef::print_private(FILE *fptr)
  {
   list<SignalDef*>::iterator i; 
   fprintf(fptr,"LINE_GENERATED(%s)\n",env.private_name().c_str());
   fprintf(fptr,"GTKMM_PRIVATE_WRAP_DECL(%s,%s,%s,%s,%s)\n",
       cppName_->c_str(),
       cppParent_->c_str(),
       cName_->c_str(),
       cParent_->c_str(),
       cCast_->c_str()
     );
   for (i=signals_.begin();i!=signals_.end();i++)
      (*i)->print_callback_decl(fptr);
   fprintf(fptr,"GTKMM_PRIVATE_DATA\n");
  }

///////////////////////////////////////////////////////////////////////

SourceChannel::SourceChannel(const string &fname)
  :OutputChannel(fname)
  {
   fprintf(out_,"GTKMM_GENERATED_TAG(%s)\n",env.input_name().c_str());
   env.print_includes(out_);
  }

PHeaderChannel::PHeaderChannel(const string &fname)
  :OutputChannel(fname)
  {
   string tmp=env.firewall()+"_p";
   fprintf(out_,"GTKMM_INCLUDE_FIREWALL(%s,%s)\n",
      tmp.c_str(),env.input_name().c_str());
  }

HeaderChannel::HeaderChannel(const string &fname)
  :OutputChannel(fname)
  {
   fprintf(out_,"GTKMM_INCLUDE_FIREWALL(%s,%s)\n",
      env.firewall().c_str(),env.input_name().c_str());
  }

OutputChannel::OutputChannel(const string &fname)
  :fname_(fname)
  {
   string tmpfile;
   tmpfile+=fname+".m4";
   out_=fopen(tmpfile.c_str(),"w");
   if (!out_)
     { 
      cerr << "Could not open "<< tmpfile<<" for output. "<< endl;
      exit (-1);
     }
   fprintf(out_,"include(stage1.m4)dnl\n");
  }

OutputChannel::~OutputChannel()
  {}

void OutputChannel::process()
  {
    fprintf(out_,"%s\n",closing().c_str());
    fclose(out_);
    string sc(env.m4_invoke());
    string tmp=fname_+".m4";
    sc+=fname_+".m4 | m4 > ";
    sc+=fname_;
    if(system(sc.c_str()))
      cerr << "m4 invocation failed : " << sc << endl;
    else
      if (!env.debug()) unlink(tmp.c_str());
  }

string OutputChannel::closing() {return "";}
string HeaderChannel::closing() {return "GTKMM_CLOSE_FIREWALL";}
string PHeaderChannel::closing() {return "GTKMM_CLOSE_FIREWALL";}

///////////////////////////////////////////////////////////////////

extern int yylineno;
Environment::Environment()
  : name_("test"),prefix_("gtkmm_"),
    srcdir_("."), destdir_("."),
    local_(false),doc_(false),debug_(false),gnome_(false)
  {}

Environment::~Environment()
  {}

void Environment::usage()
  {
   cerr << "Usage: gensig2 [options] name srcdir destdir" << endl
        << "    -h" <<endl
        << "    --help   This usage message." <<endl
        << endl
        << "    --doc    Produces a header file for documentation." <<endl
        << endl
        << "    --debug  Leave intermediate output arround for analysis." <<endl
        << endl
        << "    --m4 dir Specify the directory with m4 files." <<endl
        << endl
        << "    -l" <<endl
        << "    --local  Local widget (not part of gtk--)." <<endl
        << endl
        << "    -g" <<endl
        << "    --gnome  Prefix with GNOME instead of GTKMM." <<endl
        << endl
        << "Note: This will read srcdir/name.gen_h file and generates destdir/name.cc" <<endl;
    exit(0);
  };

void Environment::init(int argc,char** argv)
  {
   string tmp;
   int i=1;
   if (argc<2)
     {
       cerr<< "Invalid number of arguments."<<endl;
       usage();
     }
   
   while (argv[i][0]=='-')
     {
      tmp=argv[i];
      if (tmp=="-l"||tmp=="--local") 
        {
          local_=true;
        }
      else if (tmp=="-g"||tmp=="--gnome")    
        {
          gnome_=true;
          prefix_="gnome_";
        }
      else if (tmp=="-h"||tmp=="--help")
        {
          usage();
        }
      else if (tmp=="--m4")
        { 
          i++;
          m4_include_=argv[i];
        }
      else if (tmp=="--debug")
        { 
          debug_=true;
        }
      else if (tmp=="--doc")
        { 
          doc_=true;
        }
      else
        {
          cerr<<"Unknown argument "<<tmp<<endl;
        }
      i++;
      if (i==argc) 
        {
          cerr<< "Invalid number of arguments."<<endl;
          usage();
        }
     }

   name_=argv[i];
   if (i+1<argc) srcdir_=argv[i+1];
   if (i+2<argc) destdir_=argv[i+2];

  }

void Environment::print_includes(FILE *fptr)
  {
    if (local_)
      {
        fprintf(fptr,"#include \"%s.h\"\n",name_.c_str());
        fprintf(fptr,"#include \"private/%s_p.h\"\n",name_.c_str());
      }
    else if (gnome_)
      {
        fprintf(fptr,"#include <gnome--/%s.h>\n",name_.c_str());
        fprintf(fptr,"#include <gnome--/private/%s_p.h>\n",name_.c_str());
      } 
    fprintf(fptr,"#include <gtk--/%s.h>\n",name_.c_str());
    fprintf(fptr,"#include <gtk--/private/%s_p.h>\n",name_.c_str());
  }

void Environment::print_linenum(FILE *fptr)
  {
   fprintf(fptr,"#line %d %s\n",yylineno,input_name().c_str());
  }

string Environment::m4_invoke()
  {
   if (m4_include_=="")
     return "m4 ";
   else 
     return "m4 -I "+m4_include_+" ";
  }
