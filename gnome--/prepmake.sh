#! /bin/sh

for SOURCEFILE in *.gen_h
do
DESTBASE=gnome--/`basename $SOURCEFILE .gen_h`
CCFILE=$DESTBASE.cc
HFILE=$DESTBASE.h
echo "Generating $CCFILE and $HFILE from $SOURCEFILE"
../src/gensig/gensig -g $SOURCEFILE $DESTBASE
done

