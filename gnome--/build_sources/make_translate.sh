#!/bin/sh

M4FILESPATH=../../src/build_sources

# rm -f translate.m4
# touch translate.m4
# echo "GTK_TRANSLATE_START(gtk_tranlate_block)" >> translate.m4
# grep 'WRAP_CLASS' ../*.gen_h | sed 's/.*: *WRAP_CLASS(//' | awk -F, '{printf "GTK_TRANSLATE(%-20s,%s,%s,%s)\n",$1,$2,$3,$4}' | sort >> translate.m4
# echo "GTK_TRANSLATE_END()" >> translate.m4

awk -f $M4FILESPATH/getwraps.awk ../*.gen_h > translate.m4

cat $M4FILESPATH/header.m4 translate.m4 | m4 > ../gnome--/translate.h
cat $M4FILESPATH/impl.m4 translate.m4 | m4 > ../gnome--/impl.h
