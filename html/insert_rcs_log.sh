#!/bin/sh
(cd .. ; rcs2log -r -d "`cat html/relnotes_last_updated.txt`" |(echo "<pre>" ; cat ; echo "</pre>") |(cd html ;./insert_to_relnotes.pl))
mv relnotes_last_updated.txt relnotes_last_updated.old
date +">%Y-%m-%d" >relnotes_last_updated.txt
