// Does the window class even function??

#include <gdk/gdk.h>
#include "drawable.h"
#include "window.h"

gint attributes_mask;
Gdk_Window_Attr attributes;
Gdk_Window window;
Gdk_GC gc;
Gdk_Event *event;

main(int argc, char* argv[])
{
  gdk_init(&argc,&argv);

  attributes.title = "hello";
  attributes.window_type = GDK_WINDOW_TOPLEVEL;
  attributes.wmclass_name = "hello";
  attributes.wmclass_class = 0;
  attributes.width = 100;
  attributes.height = 100;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gdk_visual_get_system();
  attributes.colormap = gdk_colormap_get_system();
  attributes.event_mask = (GDK_EXPOSURE_MASK |
                            GDK_KEY_PRESS_MASK |
                            GDK_ENTER_NOTIFY_MASK |
                            GDK_LEAVE_NOTIFY_MASK |
                            GDK_FOCUS_CHANGE_MASK |
                            GDK_STRUCTURE_MASK);

  attributes_mask = GDK_WA_VISUAL | GDK_WA_COLORMAP;
  attributes_mask |= GDK_WA_TITLE;
  
  window=Gdk_Window(NULL,attributes,attributes_mask);
  window.show();

  while(1)
    {event=gdk_event_get();
     if (event) gdk_event_free(event);
    }
}

